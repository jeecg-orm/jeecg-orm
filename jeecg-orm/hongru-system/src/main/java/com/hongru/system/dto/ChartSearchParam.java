package com.hongru.system.dto;

import lombok.Data;

/**
 * @Description
 * @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
 * @Url https://www.xinhongru.com
 * @ClassName ChartSearchParam
 * @Author salter <salter@vip.163.com>
 * @Version V1.0.0
 * @Since 1.0
 * @Date 2022/5/7 17:27
 */
@Data
public class ChartSearchParam {

    String period;
    String beginDate;
    String endDate;
    String tableName;

}
