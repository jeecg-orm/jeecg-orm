package com.hongru.system.service;


import com.hongru.system.entity.SysCategory;

import java.util.List;

public interface SysCategoryService {

    void deleteByIds(String id);

    SysCategory treeRoot(SysCategory rootMenu, List<SysCategory> sourceList);
}
