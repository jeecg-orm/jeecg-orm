package com.hongru.system.entity;

import com.hongru.ebean.HongRuEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.hongru.aspect.annotation.Dict;
/**
 * @Description
 * @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
 * @Url https://www.xinhongru.com
 * @ClassName LbsController
 * @Author salter <salter@vip.163.com>
 * @Version V1.0.0
 * @Since 1.0
 * @Date 2022/1/26 15:12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Entity
@Table(name = "sys_message")
@ApiModel("我的消息对象")
public class SysMessage extends HongRuEntity {


    @ApiModelProperty(value = "消息内容",example = "",readOnly = true  )
    private String content;

    @ApiModelProperty(value = "消息标题",example = "",readOnly = true  )
    private String title;

    @ApiModelProperty(value = "状态(0:未读 1:已读 )",example = "0",readOnly = true )
    @Dict(dicCode = "read_status")
    private Boolean status;

    @ApiModelProperty(value = "优先级(L:低 M:中 H:高 )",example = "L",readOnly = true )
    @Dict(dicCode = "priority")
    private String priority;

    @ApiModelProperty(value = "业务id",example = "",readOnly = true  )
    private String busId;

    @Override
    public void save() {
        if(null==this.content){
            this.content="";
        }
        if(null==this.title){
            this.title="";
        }
        if(null==this.status){
            this.status=0==1;
        }
        if(null==this.priority){
            this.priority="L";
        }
        if(null==this.busId){
            this.busId="";
        }
        super.save();
    }


}
