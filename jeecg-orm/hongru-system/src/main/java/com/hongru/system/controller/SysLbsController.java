package com.hongru.system.controller;

import com.alibaba.fastjson.JSONObject;
import com.hongru.aspect.annotation.Log;
import com.hongru.ebean.HongRuPage;
import com.hongru.ebean.StatusDto;
import com.hongru.ebean.SortNoDto;
import com.hongru.ebean.EbeanUtil;
import com.hongru.system.entity.SysLbs;
import com.hongru.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.DynamicParameter;
import com.github.xiaoymin.knife4j.annotations.DynamicParameters;

/**
 * @Description
 * @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
 * @Url https://www.xinhongru.com
 * @ClassName LbsController
 * @Author salter <salter@vip.163.com>
 * @Version V1.0.0
 * @Since 1.0
 * @Date 2022/1/26 15:12
 */
@RestController
@RequestMapping("/sys/lbs")
@Slf4j
@ApiIgnore
public class SysLbsController {

    @PostMapping("/list")
    @ApiOperation("位置服务列表")
    @ApiOperationSupport(params = @DynamicParameters(properties = {
            @DynamicParameter(name = "name", value = "项目名称",example = "北京"),
            @DynamicParameter(name = "status", value = "状态(1:是 0:否 )",example = "1"),
    }))
    public Result<HongRuPage<SysLbs>> queryLbsPageList(@RequestBody JSONObject searchObj) {
        return Result.OK(EbeanUtil.pageList(searchObj, SysLbs.class));
    }

    @GetMapping("/{id}")
    @ApiOperation("详情")
    public Result<SysLbs> queryLbsById(@PathVariable String id) {
        SysLbs Lbs = EbeanUtil.initExpressionList(SysLbs.class).idEq(id).findOne();
        return Result.OK(Lbs);
    }

    @ApiOperation("添加")
    @ApiOperationSupport(ignoreParameters = {"sysLbs.id"})
    @PostMapping(value = "/add")
    @Log(value = "位置服务-添加")
    public Result<SysLbs> add(@RequestBody SysLbs sysLbs) {
        sysLbs.save();
        return Result.OK("添加成功");
    }

    @PostMapping(value = "/edit")
    @Log(value = "位置服务-编辑")
    public Result<SysLbs> edit(@RequestBody SysLbs sysLbs) {
        sysLbs.update();
        return Result.OK("编辑成功");
    }

    @DeleteMapping(value = "/delete")
    @Log(value = "位置服务-删除")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        EbeanUtil.delete(id, SysLbs.class);
        return Result.OK("删除成功");
    }
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        EbeanUtil.deleteBatch(ids, SysLbs.class);
        return Result.OK("批量删除成功");
    }

    @PostMapping(value="/field/{field}")
    public Result<?> field(@PathVariable String field, @RequestBody StatusDto statusDto) {
        EbeanUtil.field(field,statusDto,SysLbs.class);
        return Result.OK();
    }

    @PostMapping(value="/sortNo")
    public Result<?> sortNo(@RequestBody SortNoDto sortNoDto) {
        EbeanUtil.sortNo(sortNoDto,SysLbs.class);
        return Result.OK();
    }





}

