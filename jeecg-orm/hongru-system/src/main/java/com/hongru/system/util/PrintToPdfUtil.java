package com.hongru.system.util;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfWriter;

/**
 * @Description
 * @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
 * @Url https://www.xinhongru.com
 * @ClassName PrintToPdfUtil
 * @Author salter <salter@vip.163.com>
 * @Version V1.0.0
 * @Since 1.0
 * @Date 2022/4/20 15:05
 */
public class PrintToPdfUtil {
    /**
     * @param pdfPath         PDF文件保存地址
     */
    public static void toPdf(String[] imagePathList, String pdfPath) {
        try {
            FileOutputStream fos = new FileOutputStream(pdfPath);
            // 创建文档
            Document doc = new Document(null, 0, 0, 0, 0);
            //doc.open();
            // 写入PDF文档
            PdfWriter.getInstance(doc, fos);
            // 读取图片流
            BufferedImage img = null;
            // 实例化图片
            Image image = null;
            // 循环获取图片文件夹内的图片
            for (String imagePath : imagePathList) {
                // 读取图片流
                img = ImageIO.read(new File(imagePath));
                // 根据图片大小设置文档大小
                doc.setPageSize(new Rectangle(img.getWidth(), img
                        .getHeight()));
                // 实例化图片
                image = Image.getInstance(imagePath);
                // 添加图片到文档
                doc.open();
                doc.add(image);
            }
            // 关闭文档
            doc.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (BadElementException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        long time1 = System.currentTimeMillis();
        String []imagePathList={"C:\\opt\\upFiles\\temp\\320dace1aa8a39ecefd8108ffcc4ce7_1647506654177.png","C:\\opt\\upFiles\\temp\\微信图片_20220407182640_1649668109456.png"};
        toPdf(imagePathList, "C:\\opt\\hebing.pdf");
        long time2 = System.currentTimeMillis();
        int time = (int) ((time2 - time1) / 1000);
        System.out.println("执行了：" + time + "秒！");
    }

}

