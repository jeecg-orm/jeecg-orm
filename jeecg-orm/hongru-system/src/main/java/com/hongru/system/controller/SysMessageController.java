package com.hongru.system.controller;

import com.alibaba.fastjson.JSONObject;
import com.hongru.aspect.annotation.Log;
import com.hongru.ebean.StatusDto;
import com.hongru.ebean.SortNoDto;
import com.hongru.ebean.EbeanUtil;
import com.hongru.ebean.HongRuPage;
import com.hongru.system.entity.SysMessage;
import com.hongru.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.DynamicParameter;
import com.github.xiaoymin.knife4j.annotations.DynamicParameters;

/**
* @Description
* @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
* @Url https://www.xinhongru.com
* @ClassName LbsController
* @Author salter <salter@vip.163.com>
* @Version V1.0.0
* @Since 1.0
* @Date 2022/1/26 15:12
*/
@RestController
@RequestMapping("/sys/message")
@Slf4j
@ApiIgnore
public class SysMessageController {

    @PostMapping("/list")
    @ApiOperation("我的消息列表")
    @ApiOperationSupport(params = @DynamicParameters(properties = {
                @DynamicParameter(name = "createTime", value = "创建时间",example = ""),
                @DynamicParameter(name = "status", value = "状态(0:未读 1:已读 )",example = "0"),
                @DynamicParameter(name = "priority", value = "优先级(L:低 M:中 H:高 )",example = "L"),
    }))
    public Result<HongRuPage<SysMessage>> queryMessagePageList(@RequestBody JSONObject searchObj) {
        return Result.OK(EbeanUtil.pageList(searchObj, SysMessage.class));
    }

    @GetMapping("/{id}")
    @ApiOperation("详情")
    public Result<SysMessage> queryMessageById(@PathVariable String id) {
        SysMessage Message = EbeanUtil.initExpressionList(SysMessage.class).idEq(id).findOne();
        return Result.OK(Message);
    }

    @ApiOperation("添加")
    @ApiOperationSupport(ignoreParameters = {"sysMessage.id"})
    @PostMapping(value = "/add")
    @Log(value = "我的消息-添加")
    public Result<SysMessage> add(@RequestBody SysMessage sysMessage) {
        sysMessage.save();
        return Result.OK("添加成功");
    }

    @PostMapping(value = "/edit")
    @Log(value = "我的消息-编辑")
    public Result<SysMessage> edit(@RequestBody SysMessage sysMessage) {
        sysMessage.update();
        return Result.OK("编辑成功");
    }

    @DeleteMapping(value = "/delete")
    @Log(value = "我的消息-删除")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        EbeanUtil.delete(id, SysMessage.class);
        return Result.OK("删除成功");
    }
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        EbeanUtil.deleteBatch(ids, SysMessage.class);
        return Result.OK("批量删除成功");
    }

    @PostMapping(value="/field/{field}")
    public Result<?> field(@PathVariable String field, @RequestBody StatusDto statusDto) {
        EbeanUtil.field(field,statusDto,SysMessage.class);
        return Result.OK();
    }

    @PostMapping(value="/sortNo")
    public Result<?> sortNo(@RequestBody SortNoDto sortNoDto) {
        EbeanUtil.sortNo(sortNoDto,SysMessage.class);
        return Result.OK();
    }





}

