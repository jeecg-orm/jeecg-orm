package com.hongru.system.entity;

import com.hongru.ebean.HongRuEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.hongru.aspect.annotation.Dict;
/**
* @Description
* @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
* @Url https://www.xinhongru.com
* @ClassName LbsController
* @Author salter <salter@vip.163.com>
* @Version V1.0.0
* @Since 1.0
* @Date 2022/1/26 15:12
*/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Entity
@Table(name = "sys_log")
@ApiModel("日志对象")
public class SysLog extends HongRuEntity {


    @ApiModelProperty(value = "操作类型(add:添加 edit:编辑 delete:删除 )",example = "add",readOnly = true)
    @Dict(dicCode = "operate_type")
    private String operateType;

    @ApiModelProperty(value = "日志内容",example = "",readOnly = true)
    private String content;

    @ApiModelProperty(value = "IP",example = "",readOnly = true)
    private String ip;

    @Override
    public void save() {
       if(null==this.operateType){
        this.operateType="";
       }
       if(null==this.content){
        this.content="";
       }
       if(null==this.ip){
        this.ip="";
       }
        super.save();
    }


}
