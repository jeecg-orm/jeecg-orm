package com.hongru.system.controller;

import com.alibaba.fastjson.JSONObject;
import com.hongru.ebean.StatusDto;
import com.hongru.ebean.SortNoDto;
import com.hongru.ebean.EbeanUtil;
import com.hongru.ebean.HongRuPage;
import com.hongru.system.entity.SysLog;
import com.hongru.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.DynamicParameter;
import com.github.xiaoymin.knife4j.annotations.DynamicParameters;

/**
* @Description
* @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
* @Url https://www.xinhongru.com
* @ClassName LbsController
* @Author salter <salter@vip.163.com>
* @Version V1.0.0
* @Since 1.0
* @Date 2022/1/26 15:12
*/
@RestController
@RequestMapping("/sys/log")
@Slf4j
@ApiIgnore
public class SysLogController {

    @PostMapping("/list")
    @ApiOperation("日志列表")
    @ApiOperationSupport(params = @DynamicParameters(properties = {
                @DynamicParameter(name = "operateType", value = "操作类型(add:添加 edit:编辑 delete:删除 )",example = "add"),
                @DynamicParameter(name = "content", value = "日志内容",example = ""),
    }))
    public Result<HongRuPage<SysLog>> queryLogPageList(@RequestBody JSONObject searchObj) {
        return Result.OK(EbeanUtil.pageList(searchObj, SysLog.class));
    }

    @GetMapping("/{id}")
    @ApiOperation("详情")
    public Result<SysLog> queryLogById(@PathVariable String id) {
        SysLog Log = EbeanUtil.initExpressionList(SysLog.class).idEq(id).findOne();
        return Result.OK(Log);
    }

    @ApiOperation("添加")
    @ApiOperationSupport(ignoreParameters = {"sysLog.id"})
    @PostMapping(value = "/add")
    public Result<SysLog> add(@RequestBody SysLog sysLog) {
        sysLog.save();
        return Result.OK("添加成功");
    }

    @PostMapping(value = "/edit")
    public Result<SysLog> edit(@RequestBody SysLog sysLog) {
        sysLog.update();
        return Result.OK("修改成功");
    }

    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        EbeanUtil.delete(id, SysLog.class);
        return Result.OK("删除成功");
    }
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        EbeanUtil.deleteBatch(ids, SysLog.class);
        return Result.OK("批量删除成功");
    }

    @PostMapping(value="/field/{field}")
    public Result<?> field(@PathVariable String field, @RequestBody StatusDto statusDto) {
        EbeanUtil.field(field,statusDto,SysLog.class);
        return Result.OK();
    }

    @PostMapping(value="/sortNo")
    public Result<?> sortNo(@RequestBody SortNoDto sortNoDto) {
        EbeanUtil.sortNo(sortNoDto,SysLog.class);
        return Result.OK();
    }





}

