package com.hongru.system.controller.api;

import com.alibaba.fastjson.JSONObject;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.DynamicParameter;
import com.github.xiaoymin.knife4j.annotations.DynamicParameters;
import com.hongru.constant.CommonConstant;
import com.hongru.constant.DictConstant;
import com.hongru.ebean.EbeanUtil;
import com.hongru.ebean.HongRuPage;
import com.hongru.mms.entity.dto.SendSmsDto;
import com.hongru.mms.service.MmsSmsService;
import com.hongru.system.entity.SysCategory;
import com.hongru.system.entity.SysLbs;
import com.hongru.system.service.SysCategoryService;
import com.hongru.util.BeanUtil;
import com.hongru.util.CommonUtils;
import com.hongru.util.StringUtil;
import com.hongru.vo.Result;
import io.ebean.DB;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Description
 * @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
 * @Url https://www.xinhongru.com
 * @ClassName SysApiController
 * @Author salter <salter@vip.163.com>
 * @Version V1.0.0
 * @Since 1.0
 * @Date 2022/4/1 10:36
 */

@RestController
@RequestMapping("/sys")
@Slf4j
@Api(tags = "系统通用接口")
public class SysApiController {
    @Autowired
    private SysCategoryService categoryService;

    @Autowired
    private MmsSmsService smsService;

    @Value(value = "${hongru.path.upload}")
    private String uploadpath;

    /**
     * 本地：local minio：minio 阿里：alioss
     */
    @Value(value = "${hongru.uploadType}")
    private String uploadType;

    @Value(value = "${jo.domain}")
    private String domain;

    @PostMapping
    @ApiOperation("短信发送")
    public Result sendSms(@RequestBody SendSmsDto sendSmsDto) {
        return smsService.sendSms(sendSmsDto);
    }

    @PostMapping("/queryLbsPageList")
    @ApiOperation("位置服务列表")
    @ApiOperationSupport(params = @DynamicParameters(properties = {
            @DynamicParameter(name = "name", value = "项目名称", example = "北京"),
    }))
    public Result<HongRuPage<SysLbs>> queryLbsPageList(@RequestBody JSONObject searchObj) {
        searchObj.put("status", DictConstant.IS_OPEN_1);
        return Result.OK(EbeanUtil.pageList(searchObj, SysLbs.class));
    }

    @GetMapping("/queryLbsById/{id}")
    @ApiOperation("位置服务详情")
    public Result<SysLbs> queryLbsById(@PathVariable String id) {
        SysLbs lbs = EbeanUtil.initExpressionList(SysLbs.class).idEq(id).findOne();
        String detail = lbs.getDetailCms().replaceAll("jeecg-orm", domain);
        lbs.setDetailCms(detail);
        return Result.OK(lbs);
    }

    @GetMapping("/queryCategoryTreeList")
    @ApiOperation("分类接口")
    @ApiOperationSupport(params = @DynamicParameters(properties = {
            @DynamicParameter(name = "pcode", value = "父分类Code", example = "F01"),
    }))
    public Result<List<SysCategory>> queryCategoryTreeList(@RequestParam(name = "pcode", required = false) String pcode) {
        SysCategory pCategory = DB.find(SysCategory.class).where().eq("code", pcode).findOne();
        //获取所有菜单
        List<SysCategory> list = EbeanUtil.initExpressionList(SysCategory.class).where().orderBy("sortNo desc").findList();
        //获取root菜单
        List<SysCategory> collect = list.stream().filter(s -> pCategory.getId().equals(s.getPid())).collect(Collectors.toList());
        List<SysCategory> copyList = BeanUtil.copyList(collect, SysCategory.class);
        //遍历用户父菜单
        for (SysCategory menuVo : copyList) {
            categoryService.treeRoot(menuVo, list);
        }
        return Result.OK(copyList);
    }


    /**
     * @return
     * @Author 政辉
     */
    @GetMapping("/403")
    public Result<?> noauth() {
        return Result.error("没有权限，请联系管理员授权");
    }

    /**
     * 文件上传统一方法
     *
     * @param request
     * @return
     */
    @PostMapping(value = "/common/upload")
    @ApiOperation("文件上传")
    public Result<?> upload(HttpServletRequest request) {
        Result<?> result = new Result<>();
        String savePath = "";
        String bizPath = request.getParameter("biz");
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        MultipartFile file = multipartRequest.getFile("file");// 获取上传文件对象
        if (StringUtil.isEmpty(bizPath)) {
            if (CommonConstant.UPLOAD_TYPE_OSS.equals(uploadType)) {
                //未指定目录，则用阿里云默认目录 upload
                bizPath = "upload";
                //result.setMessage("使用阿里云文件上传时，必须添加目录！");
                //result.setSuccess(false);
                //return result;
            } else {
                bizPath = "";
            }
        }
        if (CommonConstant.UPLOAD_TYPE_LOCAL.equals(uploadType)) {
            //update-begin-author:lvdandan date:20200928 for:修改JEditor编辑器本地上传
            savePath = this.uploadLocal(file, bizPath);
            //update-begin-author:lvdandan date:20200928 for:修改JEditor编辑器本地上传
            /**  富文本编辑器及markdown本地上传时，采用返回链接方式
             //针对jeditor编辑器如何使 lcaol模式，采用 base64格式存储
             String jeditor = request.getParameter("jeditor");
             if(oConvertUtils.isNotEmpty(jeditor)){
             result.setMessage(CommonConstant.UPLOAD_TYPE_LOCAL);
             result.setSuccess(true);
             return result;
             }else{
             savePath = this.uploadLocal(file,bizPath);
             }
             */
        } else {
            //update-begin-author:taoyan date:20200814 for:文件上传改造
            savePath = CommonUtils.upload(file, bizPath, uploadType);
            //update-end-author:taoyan date:20200814 for:文件上传改造
        }
        if (StringUtil.isNotEmpty(savePath)) {
            result.setMessage(savePath);
            result.setSuccess(true);
        } else {
            result.setMessage("上传失败！");
            result.setSuccess(false);
        }
        return result;
    }

    /**
     * 本地文件上传
     *
     * @param mf      文件
     * @param bizPath 自定义路径
     * @return
     */
    private String uploadLocal(MultipartFile mf, String bizPath) {
        try {
            String ctxPath = uploadpath;
            String fileName = null;
            File file = new File(ctxPath + File.separator + bizPath + File.separator);
            if (!file.exists()) {
                file.mkdirs();// 创建文件根目录
            }
            String orgName = mf.getOriginalFilename();// 获取文件名
            orgName = CommonUtils.getFileName(orgName);
            if (orgName.indexOf(".") != -1) {
                fileName = orgName.substring(0, orgName.lastIndexOf(".")) + "_" + System.currentTimeMillis() + orgName.substring(orgName.lastIndexOf("."));
            } else {
                fileName = orgName + "_" + System.currentTimeMillis();
            }
            String savePath = file.getPath() + File.separator + fileName;
            File savefile = new File(savePath);
            FileCopyUtils.copy(mf.getBytes(), savefile);
            String dbpath = null;
            if (StringUtil.isNotEmpty(bizPath)) {
                dbpath = bizPath + File.separator + fileName;
            } else {
                dbpath = fileName;
            }
            if (dbpath.contains("\\")) {
                dbpath = dbpath.replace("\\", "/");
            }
            return dbpath;
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

}
