package com.hongru.job;

import com.hongru.mms.entity.MmsSiteMessage;
import com.hongru.mms.entity.dto.SendSmsDto;
import com.hongru.mms.service.MmsSiteMessageService;
import com.hongru.mms.service.MmsSmsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
public class AutoJob {

    @Autowired
    private MmsSmsService mmsSmsService;
    @Autowired
    private MmsSiteMessageService mmsSiteMessageService;

    //短信定时任务
//    @Scheduled(cron = "0 */1 * * * ?")
    public void autoCloseOrderProcess() {
        long startTime = System.currentTimeMillis();
        log.info("msg:{},短信定时任务执行", startTime);
        try {
            List<SendSmsDto> list = new ArrayList<>();
            for (SendSmsDto sendSmsDto : list) {
                try {
                    mmsSmsService.sendSms(sendSmsDto);
                } catch (Exception e) {
                    log.error("短信定时任务执行：{}，异常：", sendSmsDto.getSmsTemplateId(), e);
                }
            }
        } finally {
        }
        log.info("msg:{},短信定时任务执行结束,耗时:[{}]", startTime, System.currentTimeMillis() - startTime);
    }

    //站内信定时任务
//    @Scheduled(cron = "0 */1 * * * ?")
    public void autoSendCloseOrderEmail() {
        long startTime = System.currentTimeMillis();
        log.info("msg:{},站内信定时任务执行", startTime);
        try {
            List<MmsSiteMessage> list = mmsSiteMessageService.getMmsSiteMessageList();
            for (MmsSiteMessage mmsSiteMessage : list) {
                try {
                    mmsSiteMessageService.sendSiteMessage(mmsSiteMessage);
                } catch (Exception e) {
                    log.error("站内信定时任务：{}，异常：", mmsSiteMessage.getId(), e);
                }
            }
        } finally {
        }
        log.info("msg:{},站内信定时任务执行结束,耗时:[{}]", startTime, System.currentTimeMillis() - startTime);
    }


}