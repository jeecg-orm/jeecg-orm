package com.hongru.mms.controller;

import com.alibaba.fastjson.JSONObject;
import com.hongru.aspect.annotation.Log;
import com.hongru.ebean.StatusDto;
import com.hongru.ebean.SortNoDto;
import com.hongru.ebean.EbeanUtil;
import com.hongru.ebean.HongRuPage;
import com.hongru.mms.entity.MmsSiteMessageMember;
import com.hongru.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.DynamicParameter;
import com.github.xiaoymin.knife4j.annotations.DynamicParameters;

/**
* @Description
* @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
* @Url https://www.xinhongru.com
* @ClassName LbsController
* @Author salter <salter@vip.163.com>
* @Version V1.0.0
* @Since 1.0
* @Date 2022/1/26 15:12
*/
@RestController
@RequestMapping("/mms/site/message/member")
@Slf4j
@ApiIgnore
public class MmsSiteMessageMemberController {

    @PostMapping("/list")
    @ApiOperation("站内信会员记录表列表")
    @ApiOperationSupport(params = @DynamicParameters(properties = {
                @DynamicParameter(name = "title", value = "消息标题",example = ""),
    }))
    public Result<HongRuPage<MmsSiteMessageMember>> querySiteMessageMemberPageList(@RequestBody JSONObject searchObj) {
        return Result.OK(EbeanUtil.pageList(searchObj, MmsSiteMessageMember.class));
    }

    @GetMapping("/{id}")
    @ApiOperation("详情")
    public Result<MmsSiteMessageMember> querySiteMessageMemberById(@PathVariable String id) {
        MmsSiteMessageMember SiteMessageMember = EbeanUtil.initExpressionList(MmsSiteMessageMember.class).idEq(id).findOne();
        return Result.OK(SiteMessageMember);
    }

    @ApiOperation("添加")
    @ApiOperationSupport(ignoreParameters = {"mmsSiteMessageMember.id"})
    @PostMapping(value = "/add")
    @Log(value = "站内信会员记录表-添加")
    public Result<MmsSiteMessageMember> add(@RequestBody MmsSiteMessageMember mmsSiteMessageMember) {
        mmsSiteMessageMember.save();
        return Result.OK("添加成功");
    }

    @PostMapping(value = "/edit")
    @Log(value = "站内信会员记录表-编辑")
    public Result<MmsSiteMessageMember> edit(@RequestBody MmsSiteMessageMember mmsSiteMessageMember) {
        mmsSiteMessageMember.update();
        return Result.OK("编辑成功");
    }

    @DeleteMapping(value = "/delete")
    @Log(value = "站内信会员记录表-删除")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        EbeanUtil.delete(id, MmsSiteMessageMember.class);
        return Result.OK("删除成功");
    }
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        EbeanUtil.deleteBatch(ids, MmsSiteMessageMember.class);
        return Result.OK("批量删除成功");
    }

    @PostMapping(value="/field/{field}")
    public Result<?> field(@PathVariable String field, @RequestBody StatusDto statusDto) {
        EbeanUtil.field(field,statusDto,MmsSiteMessageMember.class);
        return Result.OK();
    }

    @PostMapping(value="/sortNo")
    public Result<?> sortNo(@RequestBody SortNoDto sortNoDto) {
        EbeanUtil.sortNo(sortNoDto,MmsSiteMessageMember.class);
        return Result.OK();
    }





}

