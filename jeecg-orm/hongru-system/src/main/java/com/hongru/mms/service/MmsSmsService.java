package com.hongru.mms.service;

import com.hongru.mms.entity.dto.SendSmsDto;
import com.hongru.vo.Result;

/**
 * @Description
 * @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
 * @Url https://www.xinhongru.com
 * @ClassName MmsSmsService
 * @Author salter <salter@vip.163.com>
 * @Version V1.0.0
 * @Since 1.0
 * @Date 2022/4/22 14:29
 */
public interface MmsSmsService {
    Result sendSms(SendSmsDto sendSmsDto);
}
