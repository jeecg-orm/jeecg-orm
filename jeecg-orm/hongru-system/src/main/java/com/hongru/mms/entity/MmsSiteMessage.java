package com.hongru.mms.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hongru.aspect.annotation.Dict;
import com.hongru.ebean.HongRuEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
/**
* @Description
* @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
* @Url https://www.xinhongru.com
* @ClassName LbsController
* @Author salter <salter@vip.163.com>
* @Version V1.0.0
* @Since 1.0
* @Date 2022/1/26 15:12
*/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Entity
@Table(name = "mms_site_message")
@ApiModel("站内信对象")
public class MmsSiteMessage extends HongRuEntity {


    @ApiModelProperty(value = "消息标题",example = "")
    private String title;

    @ApiModelProperty(value = "发送内容",example = "")
    private String content;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty(value = "发送时间",example = "")
    private Date sendTime;

    @ApiModelProperty(value = "接收用户",example = "")
    private String receiveMember;

    @ApiModelProperty(value = "发送状态(0:未发送 1:已发送 )",example = "0",readOnly = true)
    @Dict(dicCode = "send_status")
    private Integer status;

    @Override
    public void save() {
        if(null==this.status){
        this.status=0;
        }
        super.save();
    }


}
