package com.hongru.mms.controller;

import com.alibaba.fastjson.JSONObject;
import com.hongru.aspect.annotation.Log;
import com.hongru.ebean.StatusDto;
import com.hongru.ebean.SortNoDto;
import com.hongru.ebean.EbeanUtil;
import com.hongru.ebean.HongRuPage;
import com.hongru.mms.entity.MmsSubscribeTemplate;
import com.hongru.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.DynamicParameter;
import com.github.xiaoymin.knife4j.annotations.DynamicParameters;

/**
* @Description
* @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
* @Url https://www.xinhongru.com
* @ClassName LbsController
* @Author salter <salter@vip.163.com>
* @Version V1.0.0
* @Since 1.0
* @Date 2022/1/26 15:12
*/
@RestController
@RequestMapping("/mms/subscribe/template")
@Slf4j
@ApiIgnore
public class MmsSubscribeTemplateController {

    @PostMapping("/list")
    @ApiOperation("订阅模板表列表")
    @ApiOperationSupport(params = @DynamicParameters(properties = {
                @DynamicParameter(name = "title", value = "标题",example = ""),
                @DynamicParameter(name = "templateId", value = "微信模板ID",example = ""),
                @DynamicParameter(name = "subscribeType", value = "模板类型(0:普通 1:课程 )",example = "0"),
    }))
    public Result<HongRuPage<MmsSubscribeTemplate>> querySubscribeTemplatePageList(@RequestBody JSONObject searchObj) {
        return Result.OK(EbeanUtil.pageList(searchObj, MmsSubscribeTemplate.class));
    }

    @GetMapping("/{id}")
    @ApiOperation("详情")
    public Result<MmsSubscribeTemplate> querySubscribeTemplateById(@PathVariable String id) {
        MmsSubscribeTemplate SubscribeTemplate = EbeanUtil.initExpressionList(MmsSubscribeTemplate.class).idEq(id).findOne();
        return Result.OK(SubscribeTemplate);
    }

    @ApiOperation("添加")
    @ApiOperationSupport(ignoreParameters = {"mmsSubscribeTemplate.id"})
    @PostMapping(value = "/add")
    @Log(value = "订阅模板表-添加")
    public Result<MmsSubscribeTemplate> add(@RequestBody MmsSubscribeTemplate mmsSubscribeTemplate) {
        mmsSubscribeTemplate.save();
        return Result.OK("添加成功");
    }

    @PostMapping(value = "/edit")
    @Log(value = "订阅模板表-编辑")
    public Result<MmsSubscribeTemplate> edit(@RequestBody MmsSubscribeTemplate mmsSubscribeTemplate) {
        mmsSubscribeTemplate.update();
        return Result.OK("编辑成功");
    }

    @DeleteMapping(value = "/delete")
    @Log(value = "订阅模板表-删除")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        EbeanUtil.delete(id, MmsSubscribeTemplate.class);
        return Result.OK("删除成功");
    }
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        EbeanUtil.deleteBatch(ids, MmsSubscribeTemplate.class);
        return Result.OK("批量删除成功");
    }

    @PostMapping(value="/field/{field}")
    public Result<?> field(@PathVariable String field, @RequestBody StatusDto statusDto) {
        EbeanUtil.field(field,statusDto,MmsSubscribeTemplate.class);
        return Result.OK();
    }

    @PostMapping(value="/sortNo")
    public Result<?> sortNo(@RequestBody SortNoDto sortNoDto) {
        EbeanUtil.sortNo(sortNoDto,MmsSubscribeTemplate.class);
        return Result.OK();
    }





}

