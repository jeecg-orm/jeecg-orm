package com.hongru.mms.entity;

import com.hongru.ebean.HongRuEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.hongru.aspect.annotation.Dict;
/**
* @Description
* @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
* @Url https://www.xinhongru.com
* @ClassName LbsController
* @Author salter <salter@vip.163.com>
* @Version V1.0.0
* @Since 1.0
* @Date 2022/1/26 15:12
*/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Entity
@Table(name = "mms_sms_template")
@ApiModel("短信模板对象")
public class MmsSmsTemplate extends HongRuEntity {


    @ApiModelProperty(value = "模板名称",example = "")
    private String name;

    @ApiModelProperty(value = "模板内容",example = "")
    private String content;

    @ApiModelProperty(value = "云服务商(qcloud:腾讯云 alicloud:阿里云 )",example = "qcloud")
    @Dict(dicCode = "cloud_type")
    private String cloudType;

    @ApiModelProperty(value = "云服务商短信code代码",example = "")
    private String cloudCode;

    @Override
    public void save() {
        super.save();
    }


}
