package com.hongru.mms.controller;

import com.alibaba.fastjson.JSONObject;
import com.hongru.aspect.annotation.Log;
import com.hongru.ebean.StatusDto;
import com.hongru.ebean.SortNoDto;
import com.hongru.ebean.EbeanUtil;
import com.hongru.ebean.HongRuPage;
import com.hongru.mms.entity.MmsSmsTask;
import com.hongru.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.DynamicParameter;
import com.github.xiaoymin.knife4j.annotations.DynamicParameters;

/**
* @Description
* @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
* @Url https://www.xinhongru.com
* @ClassName LbsController
* @Author salter <salter@vip.163.com>
* @Version V1.0.0
* @Since 1.0
* @Date 2022/1/26 15:12
*/
@RestController
@RequestMapping("/mms/sms/task")
@Slf4j
@ApiIgnore
public class MmsSmsTaskController {

    @PostMapping("/list")
    @ApiOperation("短信发送计划列表")
    @ApiOperationSupport(params = @DynamicParameters(properties = {
                @DynamicParameter(name = "smsTemplateId", value = "短信模板",example = ""),
                @DynamicParameter(name = "title", value = "短信标题",example = ""),
    }))
    public Result<HongRuPage<MmsSmsTask>> querySmsTaskPageList(@RequestBody JSONObject searchObj) {
        return Result.OK(EbeanUtil.pageList(searchObj, MmsSmsTask.class));
    }

    @GetMapping("/{id}")
    @ApiOperation("详情")
    public Result<MmsSmsTask> querySmsTaskById(@PathVariable String id) {
        MmsSmsTask SmsTask = EbeanUtil.initExpressionList(MmsSmsTask.class).idEq(id).findOne();
        return Result.OK(SmsTask);
    }

    @ApiOperation("添加")
    @ApiOperationSupport(ignoreParameters = {"mmsSmsTask.id"})
    @PostMapping(value = "/add")
    @Log(value = "短信发送计划-添加")
    public Result<MmsSmsTask> add(@RequestBody MmsSmsTask mmsSmsTask) {
        mmsSmsTask.save();
        return Result.OK("添加成功");
    }

    @PostMapping(value = "/edit")
    @Log(value = "短信发送计划-编辑")
    public Result<MmsSmsTask> edit(@RequestBody MmsSmsTask mmsSmsTask) {
        mmsSmsTask.update();
        return Result.OK("编辑成功");
    }

    @DeleteMapping(value = "/delete")
    @Log(value = "短信发送计划-删除")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        EbeanUtil.delete(id, MmsSmsTask.class);
        return Result.OK("删除成功");
    }
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        EbeanUtil.deleteBatch(ids, MmsSmsTask.class);
        return Result.OK("批量删除成功");
    }

    @PostMapping(value="/field/{field}")
    public Result<?> field(@PathVariable String field, @RequestBody StatusDto statusDto) {
        EbeanUtil.field(field,statusDto,MmsSmsTask.class);
        return Result.OK();
    }

    @PostMapping(value="/sortNo")
    public Result<?> sortNo(@RequestBody SortNoDto sortNoDto) {
        EbeanUtil.sortNo(sortNoDto,MmsSmsTask.class);
        return Result.OK();
    }





}

