package com.hongru.mms.controller;

import com.alibaba.fastjson.JSONObject;
import com.hongru.aspect.annotation.Log;
import com.hongru.ebean.StatusDto;
import com.hongru.ebean.SortNoDto;
import com.hongru.ebean.EbeanUtil;
import com.hongru.ebean.HongRuPage;
import com.hongru.mms.entity.MmsSmsLog;
import com.hongru.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.DynamicParameter;
import com.github.xiaoymin.knife4j.annotations.DynamicParameters;

/**
* @Description
* @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
* @Url https://www.xinhongru.com
* @ClassName LbsController
* @Author salter <salter@vip.163.com>
* @Version V1.0.0
* @Since 1.0
* @Date 2022/1/26 15:12
*/
@RestController
@RequestMapping("/mms/sms/log")
@Slf4j
@ApiIgnore
public class MmsSmsLogController {

    @PostMapping("/list")
    @ApiOperation("短信发送日志列表")
    @ApiOperationSupport(params = @DynamicParameters(properties = {
                @DynamicParameter(name = "createTime", value = "创建时间",example = ""),
                @DynamicParameter(name = "phone", value = "手机号",example = ""),
                @DynamicParameter(name = "smsTemplateId", value = "短信模板",example = ""),
                @DynamicParameter(name = "sendStatus", value = "接口唤起状态(1:成功 0:失败 )",example = "1"),
    }))
    public Result<HongRuPage<MmsSmsLog>> querySmsLogPageList(@RequestBody JSONObject searchObj) {
        return Result.OK(EbeanUtil.pageList(searchObj, MmsSmsLog.class));
    }

    @GetMapping("/{id}")
    @ApiOperation("详情")
    public Result<MmsSmsLog> querySmsLogById(@PathVariable String id) {
        MmsSmsLog SmsLog = EbeanUtil.initExpressionList(MmsSmsLog.class).idEq(id).findOne();
        return Result.OK(SmsLog);
    }

    @ApiOperation("添加")
    @ApiOperationSupport(ignoreParameters = {"mmsSmsLog.id"})
    @PostMapping(value = "/add")
    @Log(value = "短信发送日志-添加")
    public Result<MmsSmsLog> add(@RequestBody MmsSmsLog mmsSmsLog) {
        mmsSmsLog.save();
        return Result.OK("添加成功");
    }

    @PostMapping(value = "/edit")
    @Log(value = "短信发送日志-编辑")
    public Result<MmsSmsLog> edit(@RequestBody MmsSmsLog mmsSmsLog) {
        mmsSmsLog.update();
        return Result.OK("编辑成功");
    }

    @DeleteMapping(value = "/delete")
    @Log(value = "短信发送日志-删除")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        EbeanUtil.delete(id, MmsSmsLog.class);
        return Result.OK("删除成功");
    }
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        EbeanUtil.deleteBatch(ids, MmsSmsLog.class);
        return Result.OK("批量删除成功");
    }

    @PostMapping(value="/field/{field}")
    public Result<?> field(@PathVariable String field, @RequestBody StatusDto statusDto) {
        EbeanUtil.field(field,statusDto,MmsSmsLog.class);
        return Result.OK();
    }

    @PostMapping(value="/sortNo")
    public Result<?> sortNo(@RequestBody SortNoDto sortNoDto) {
        EbeanUtil.sortNo(sortNoDto,MmsSmsLog.class);
        return Result.OK();
    }





}

