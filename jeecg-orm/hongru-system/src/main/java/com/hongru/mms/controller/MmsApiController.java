package com.hongru.mms.controller;

import com.alibaba.fastjson.JSONObject;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.DynamicParameter;
import com.github.xiaoymin.knife4j.annotations.DynamicParameters;
import com.hongru.ebean.EbeanUtil;
import com.hongru.ebean.HongRuPage;
import com.hongru.mms.entity.MmsSubscribeTemplate;
import com.hongru.vo.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/mms")
@Slf4j
@Api(tags = "MMS接口汇总")
public class MmsApiController {

    @PostMapping("/querySubscribeTemplatePageList")
    @ApiOperation(value = "订阅模板表列表", tags = "订阅消息管理")
    @ApiOperationSupport(params = @DynamicParameters(properties = {
            @DynamicParameter(name = "subscribeType", value = "模板类型(0:普通 1:课程 )",example = "0"),
    }))
    public Result<HongRuPage<MmsSubscribeTemplate>> querySubscribeTemplatePageList(@RequestBody JSONObject searchObj) {
        return Result.OK(EbeanUtil.pageList(searchObj, MmsSubscribeTemplate.class));
    }


}
