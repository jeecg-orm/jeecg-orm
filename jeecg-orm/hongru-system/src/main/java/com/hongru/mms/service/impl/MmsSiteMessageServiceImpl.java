package com.hongru.mms.service.impl;

import com.hongru.constant.DictConstant;
import com.hongru.ebean.EbeanUtil;
import com.hongru.mms.entity.MmsSiteMessage;
import com.hongru.mms.entity.MmsSiteMessageMember;
import com.hongru.mms.service.MmsSiteMessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class MmsSiteMessageServiceImpl implements MmsSiteMessageService {

    @Async
    @Override
    public void sendSiteMessage(MmsSiteMessage mmsSiteMessage) {
        String[] memberArr = mmsSiteMessage.getReceiveMember().split(",");
        for (String memberId : memberArr) {
            MmsSiteMessageMember mmsSiteMessageMember = new MmsSiteMessageMember();
            mmsSiteMessageMember.setTitle(mmsSiteMessage.getTitle());
            mmsSiteMessageMember.setContent(mmsSiteMessage.getContent());
            mmsSiteMessageMember.setMemberId(memberId);
            mmsSiteMessageMember.setStatus(DictConstant.SEND_STATUS_1);
            mmsSiteMessageMember.save();
        }
        mmsSiteMessage.setStatus(DictConstant.SEND_STATUS_1);
        mmsSiteMessage.update();
    }

    @Override
    public List<MmsSiteMessage> getMmsSiteMessageList() {
        //查询所有问卷
        List<MmsSiteMessage> list = EbeanUtil.initExpressionList(MmsSiteMessage.class).where()
                .le("sendTime", new Date())
                .eq("status", DictConstant.SEND_STATUS_0)
                .findList();
        return list;
    }

}
