package com.hongru.mms.entity;

import com.hongru.ebean.HongRuEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.hongru.aspect.annotation.Dict;
/**
* @Description
* @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
* @Url https://www.xinhongru.com
* @ClassName LbsController
* @Author salter <salter@vip.163.com>
* @Version V1.0.0
* @Since 1.0
* @Date 2022/1/26 15:12
*/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Entity
@Table(name = "mms_sms_log")
@ApiModel("短信发送日志对象")
public class MmsSmsLog extends HongRuEntity {


    @ApiModelProperty(value = "手机号",example = ""  )
    private String phone;

    @ApiModelProperty(value = "短信内容",example = ""  )
    private String content;

    @ApiModelProperty(value = "短信模板",example = "" )
    @Dict(dicCode = "id",dictTable ="mms_sms_template",dicText = "name")
    private String smsTemplateId;

    @ApiModelProperty(value = "第三方接口请求返回结果",example = "",readOnly = true  )
    private String sendResult;

    @ApiModelProperty(value = "接口唤起状态(1:成功 0:失败 )",example = "1" )
    @Dict(dicCode = "req_status")
    private Integer sendStatus;

    @Override
    public void save() {
       if(null==this.sendResult){
        this.sendResult="";
       }
        super.save();
    }


}
