package com.hongru.mms.entity.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hongru.aspect.annotation.Dict;
import com.hongru.ebean.HongRuEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
* @Description
* @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
* @Url https://www.xinhongru.com
* @ClassName LbsController
* @Author salter <salter@vip.163.com>
* @Version V1.0.0
* @Since 1.0
* @Date 2022/1/26 15:12
*/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Entity
@Table(name = "mms_site_message_member")
@ApiModel("站内信会员记录表对象")
public class MmsSiteMessageMemberVo extends HongRuEntity {


    @ApiModelProperty(value = "消息标题",example = "")
    private String title;

    @ApiModelProperty(value = "发送内容",example = "",readOnly = true)
    private String content;

    @JsonIgnore
    @ApiModelProperty(value = "会员id",example = "",readOnly = true)
    private String memberId;

    @ApiModelProperty(value = "消息状态(0:未读 1:已读 )",example = "0",readOnly = true )
    @Dict(dicCode = "read_status")
    private Integer status;


}
