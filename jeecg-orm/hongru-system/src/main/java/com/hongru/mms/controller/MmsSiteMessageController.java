package com.hongru.mms.controller;

import com.alibaba.fastjson.JSONObject;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.DynamicParameter;
import com.github.xiaoymin.knife4j.annotations.DynamicParameters;
import com.hongru.ebean.EbeanUtil;
import com.hongru.ebean.HongRuPage;
import com.hongru.ebean.SortNoDto;
import com.hongru.ebean.StatusDto;
import com.hongru.mms.entity.MmsSiteMessage;
import com.hongru.vo.Result;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

/**
* @Description
* @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
* @Url https://www.xinhongru.com
* @ClassName LbsController
* @Author salter <salter@vip.163.com>
* @Version V1.0.0
* @Since 1.0
* @Date 2022/1/26 15:12
*/
@RestController
@RequestMapping("/mms/site/message")
@Slf4j
@ApiIgnore
public class MmsSiteMessageController {

    @PostMapping("/list")
    @ApiOperation("站内信列表")
    @ApiOperationSupport(params = @DynamicParameters(properties = {
                @DynamicParameter(name = "title", value = "消息标题",example = ""),
                @DynamicParameter(name = "sendTime", value = "发送时间",example = ""),
                @DynamicParameter(name = "status", value = "发送状态(0:未发送 1:已发送 )",example = "0"),
    }))
    public Result<HongRuPage<MmsSiteMessage>> querySiteMessagePageList(@RequestBody JSONObject searchObj) {
        return Result.OK(EbeanUtil.pageList(searchObj, MmsSiteMessage.class));
    }

    @GetMapping("/{id}")
    @ApiOperation("详情")
    public Result<MmsSiteMessage> querySiteMessageById(@PathVariable String id) {
        MmsSiteMessage SiteMessage = EbeanUtil.initExpressionList(MmsSiteMessage.class).idEq(id).findOne();
        return Result.OK(SiteMessage);
    }

    @ApiOperation("添加")
    @ApiOperationSupport(ignoreParameters = {"mmsSiteMessage.id"})
    @PostMapping(value = "/add")
    public Result<MmsSiteMessage> add(@RequestBody MmsSiteMessage mmsSiteMessage) {
        mmsSiteMessage.save();
        return Result.OK("添加成功");
    }

    @PostMapping(value = "/edit")
    public Result<MmsSiteMessage> edit(@RequestBody MmsSiteMessage mmsSiteMessage) {
        mmsSiteMessage.update();
        return Result.OK("修改成功");
    }

    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        EbeanUtil.delete(id, MmsSiteMessage.class);
        return Result.OK("删除成功");
    }
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        EbeanUtil.deleteBatch(ids, MmsSiteMessage.class);
        return Result.OK("批量删除成功");
    }

    @PostMapping(value="/field/{field}")
    public Result<?> field(@PathVariable String field, @RequestBody StatusDto statusDto) {
        EbeanUtil.field(field,statusDto,MmsSiteMessage.class);
        return Result.OK();
    }

    @PostMapping(value="/sortNo")
    public Result<?> sortNo(@RequestBody SortNoDto sortNoDto) {
        EbeanUtil.sortNo(sortNoDto,MmsSiteMessage.class);
        return Result.OK();
    }





}

