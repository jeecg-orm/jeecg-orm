package com.hongru.mms.entity;

import com.hongru.ebean.HongRuEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.hongru.aspect.annotation.Dict;
/**
* @Description
* @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
* @Url https://www.xinhongru.com
* @ClassName LbsController
* @Author salter <salter@vip.163.com>
* @Version V1.0.0
* @Since 1.0
* @Date 2022/1/26 15:12
*/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Entity
@Table(name = "mms_subscribe_template")
@ApiModel("订阅模板表对象")
public class MmsSubscribeTemplate extends HongRuEntity {


    @ApiModelProperty(value = "标题",example = "",required = true  )
    private String title;

    @ApiModelProperty(value = "微信模板ID",example = "",required = true  )
    private String templateId;

    @ApiModelProperty(value = "模板类型(0:普通 1:课程 )",example = "0",required = true )
    @Dict(dicCode = "subscribe_type")
    private Integer subscribeType;

    @Override
    public void save() {
        super.save();
    }


}
