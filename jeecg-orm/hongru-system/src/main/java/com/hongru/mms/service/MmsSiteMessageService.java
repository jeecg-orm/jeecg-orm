package com.hongru.mms.service;

import com.hongru.mms.entity.MmsSiteMessage;

import java.util.List;

public interface MmsSiteMessageService {

    void sendSiteMessage(MmsSiteMessage mmsSiteMessage);

    List<MmsSiteMessage> getMmsSiteMessageList();
}
