package com.hongru.mms.entity;

import com.hongru.ebean.HongRuEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.hongru.aspect.annotation.Dict;
/**
* @Description
* @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
* @Url https://www.xinhongru.com
* @ClassName LbsController
* @Author salter <salter@vip.163.com>
* @Version V1.0.0
* @Since 1.0
* @Date 2022/1/26 15:12
*/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Entity
@Table(name = "mms_sms_task")
@ApiModel("短信发送计划对象")
public class MmsSmsTask extends HongRuEntity {


    @ApiModelProperty(value = "短信模板",example = "" )
    @Dict(dicCode = "id",dictTable ="mms_sms_template",dicText = "name")
    private String smsTemplateId;

    @ApiModelProperty(value = "短信标题",example = ""  )
    private String title;

    @ApiModelProperty(value = "接受人",example = ""  )
    private String memberIds;

    @ApiModelProperty(value = "备注",example = ""  )
    private String remark;

    @ApiModelProperty(value = "发送状态(0:未发送 1:已发送 )",example = "0",readOnly = true )
    @Dict(dicCode = "send_status")
    private Integer sendStatus;

    @ApiModelProperty(value = "自定义接收人",example = ""  )
    private String phones;

    @Override
    public void save() {
        if(null==this.sendStatus){
        this.sendStatus=0;
        }
        super.save();
    }


}
