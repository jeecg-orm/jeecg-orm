package com.hongru.cms.entity;

import com.hongru.ebean.HongRuEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.hongru.aspect.annotation.Dict;
/**
* @Description
* @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
* @Url https://www.xinhongru.com
* @ClassName LbsController
* @Author salter <salter@vip.163.com>
* @Version V1.0.0
* @Since 1.0
* @Date 2022/1/26 15:12
*/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Entity
@Table(name = "cms_question_problem")
@ApiModel("健康问卷问题对象")
public class CmsQuestionProblem extends HongRuEntity {


    @ApiModelProperty(value = "问题名称",example = "")
    private String name;

    @ApiModelProperty(value = "问题简介",example = "")
    private String intro;

    @ApiModelProperty(value = "问题类型(4:数字 3:文本 1:单选 2:多选 )",example = "4")
    @Dict(dicCode = "problem_type")
    private String problemType;

    @ApiModelProperty(value = "选项值(|分隔)",example = "高血压 | 糖尿病 | 心血管疾病 | 感冒发烧")
    private String options;

    @ApiModelProperty(value = "问卷ID",example = "",readOnly = true)
    private String questionId;

    @ApiModelProperty(value = "状态(1:开启 0:关闭 )",example = "1",readOnly = true)
    @Dict(dicCode = "status")
    private Boolean status;

    @Override
    public void save() {
       if(null==this.questionId){
        this.questionId="";
       }
        if(null==this.status){
        this.status=0==1;
        }
        super.save();
    }


}
