package com.hongru.cms.controller;

import com.alibaba.fastjson.JSONObject;
import com.hongru.aspect.annotation.Log;
import com.hongru.ebean.StatusDto;
import com.hongru.ebean.SortNoDto;
import com.hongru.ebean.EbeanUtil;
import com.hongru.ebean.HongRuPage;
import com.hongru.cms.entity.CmsVideo;
import com.hongru.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.DynamicParameter;
import com.github.xiaoymin.knife4j.annotations.DynamicParameters;

/**
* @Description
* @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
* @Url https://www.xinhongru.com
* @ClassName LbsController
* @Author salter <salter@vip.163.com>
* @Version V1.0.0
* @Since 1.0
* @Date 2022/1/26 15:12
*/
@RestController
@RequestMapping("/cms/video")
@Slf4j
@ApiIgnore
public class CmsVideoController {

    @PostMapping("/list")
    @ApiOperation("运动视频列表")
    @ApiOperationSupport(params = @DynamicParameters(properties = {
                @DynamicParameter(name = "name", value = "视频名称",example = "颈部康复"),
                @DynamicParameter(name = "categoryCode", value = "视频类型",example = "F07"),
    }))
    public Result<HongRuPage<CmsVideo>> queryVideoPageList(@RequestBody JSONObject searchObj) {
        return Result.OK(EbeanUtil.pageList(searchObj, CmsVideo.class));
    }

    @GetMapping("/{id}")
    @ApiOperation("详情")
    public Result<CmsVideo> queryVideoById(@PathVariable String id) {
        CmsVideo Video = EbeanUtil.initExpressionList(CmsVideo.class).idEq(id).findOne();
        return Result.OK(Video);
    }

    @ApiOperation("添加")
    @ApiOperationSupport(ignoreParameters = {"cmsVideo.id"})
    @PostMapping(value = "/add")
    @Log(value = "运动视频-添加")
    public Result<CmsVideo> add(@RequestBody CmsVideo cmsVideo) {
        cmsVideo.save();
        return Result.OK("添加成功");
    }

    @PostMapping(value = "/edit")
    @Log(value = "运动视频-编辑")
    public Result<CmsVideo> edit(@RequestBody CmsVideo cmsVideo) {
        cmsVideo.update();
        return Result.OK("编辑成功");
    }

    @DeleteMapping(value = "/delete")
    @Log(value = "运动视频-删除")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        EbeanUtil.delete(id, CmsVideo.class);
        return Result.OK("删除成功");
    }
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        EbeanUtil.deleteBatch(ids, CmsVideo.class);
        return Result.OK("批量删除成功");
    }

    @PostMapping(value="/field/{field}")
    public Result<?> field(@PathVariable String field, @RequestBody StatusDto statusDto) {
        EbeanUtil.field(field,statusDto,CmsVideo.class);
        return Result.OK();
    }

    @PostMapping(value="/sortNo")
    public Result<?> sortNo(@RequestBody SortNoDto sortNoDto) {
        EbeanUtil.sortNo(sortNoDto,CmsVideo.class);
        return Result.OK();
    }





}

