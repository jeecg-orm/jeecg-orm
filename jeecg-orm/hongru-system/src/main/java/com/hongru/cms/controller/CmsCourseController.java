package com.hongru.cms.controller;

import com.alibaba.fastjson.JSONObject;
import com.hongru.aspect.annotation.Log;
import com.hongru.ebean.StatusDto;
import com.hongru.ebean.SortNoDto;
import com.hongru.ebean.EbeanUtil;
import com.hongru.ebean.HongRuPage;
import com.hongru.cms.entity.CmsCourse;
import com.hongru.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.DynamicParameter;
import com.github.xiaoymin.knife4j.annotations.DynamicParameters;

/**
* @Description
* @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
* @Url https://www.xinhongru.com
* @ClassName LbsController
* @Author salter <salter@vip.163.com>
* @Version V1.0.0
* @Since 1.0
* @Date 2022/1/26 15:12
*/
@RestController
@RequestMapping("/cms/course")
@Slf4j
@ApiIgnore
public class CmsCourseController {

    @PostMapping("/list")
    @ApiOperation("健康课堂列表")
    @ApiOperationSupport(params = @DynamicParameters(properties = {
                @DynamicParameter(name = "name", value = "课程名称",example = "专家教您如何守护孩子脊柱健康"),
                @DynamicParameter(name = "isHot", value = "是否热门(0:否 1:是 )",example = "0"),
                @DynamicParameter(name = "status", value = "是否发布(0:否 1:是 )",example = "0"),
    }))
    public Result<HongRuPage<CmsCourse>> queryCoursePageList(@RequestBody JSONObject searchObj) {
        return Result.OK(EbeanUtil.pageList(searchObj, CmsCourse.class));
    }

    @GetMapping("/{id}")
    @ApiOperation("详情")
    public Result<CmsCourse> queryCourseById(@PathVariable String id) {
        CmsCourse Course = EbeanUtil.initExpressionList(CmsCourse.class).idEq(id).findOne();
        return Result.OK(Course);
    }

    @ApiOperation("添加")
    @ApiOperationSupport(ignoreParameters = {"cmsCourse.id"})
    @PostMapping(value = "/add")
    @Log(value = "健康课堂-添加")
    public Result<CmsCourse> add(@RequestBody CmsCourse cmsCourse) {
        cmsCourse.save();
        return Result.OK("添加成功");
    }

    @PostMapping(value = "/edit")
    @Log(value = "健康课堂-编辑")
    public Result<CmsCourse> edit(@RequestBody CmsCourse cmsCourse) {
        cmsCourse.update();
        return Result.OK("编辑成功");
    }

    @DeleteMapping(value = "/delete")
    @Log(value = "健康课堂-删除")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        EbeanUtil.delete(id, CmsCourse.class);
        return Result.OK("删除成功");
    }
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        EbeanUtil.deleteBatch(ids, CmsCourse.class);
        return Result.OK("批量删除成功");
    }

    @PostMapping(value="/field/{field}")
    public Result<?> field(@PathVariable String field, @RequestBody StatusDto statusDto) {
        EbeanUtil.field(field,statusDto,CmsCourse.class);
        return Result.OK();
    }

    @PostMapping(value="/sortNo")
    public Result<?> sortNo(@RequestBody SortNoDto sortNoDto) {
        EbeanUtil.sortNo(sortNoDto,CmsCourse.class);
        return Result.OK();
    }





}

