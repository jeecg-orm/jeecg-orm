package com.hongru.cms.entity;

import com.hongru.ebean.HongRuEntity;
import com.hongru.util.JwtUtil;
import com.hongru.vo.SysUserCacheInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.hongru.aspect.annotation.Dict;
/**
* @Description
* @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
* @Url https://www.xinhongru.com
* @ClassName LbsController
* @Author salter <salter@vip.163.com>
* @Version V1.0.0
* @Since 1.0
* @Date 2022/1/26 15:12
*/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Entity
@Table(name = "cms_content")
@ApiModel("内容对象")
public class CmsContent extends HongRuEntity {


    @ApiModelProperty(value = "栏目编码",example = "C01",readOnly = true  )
    private String channelCode;

    @ApiModelProperty(value = "标题",example = "首页",required = true  )
    private String title;

    @ApiModelProperty(value = "副标题",example = "0"  )
    private String subTitle;

    @ApiModelProperty(value = "简介",example = "0"  )
    private String intro;

    @ApiModelProperty(value = "图片",example = "0"  )
    private String image;

    @ApiModelProperty(value = "图片1",example = ""  )
    private String image1;

    @ApiModelProperty(value = "文件",example = "0"  )
    private String file;

    @ApiModelProperty(value = "富文本",example = "0"  )
    private String detailCms;

    @ApiModelProperty(value = "跳转页面",example = "0" )
    @Dict(dicCode = "id",dictTable ="sys_link",dicText = "name")
    private String linkId;

    @ApiModelProperty(value = "跳转数据ID",example = "0"  )
    private String dataId;

    @ApiModelProperty(value = "状态(0:关闭 1:开启 )",example = "0",readOnly = true )
    @Dict(dicCode = "status")
    private Boolean status;

    @ApiModelProperty(value = "分类",example = "F01",required = true )
    @Dict(dicCode = "code",dictTable ="sys_category",dicText = "name")
    private String categoryCode;

    @ApiModelProperty(value = "是否推荐(0:否 1:是 )",example = "0",readOnly = true )
    @Dict(dicCode = "is_open")
    private Boolean isHot;

    @ApiModelProperty(value = "项目ID",example = "",readOnly = true  )
    private String lbsId;

    @ApiModelProperty(value = "列表展现形式(1:文字 2:图文 3:视频 4:音频 )",example = "1" )
    @Dict(dicCode = "content_list_type")
    private Integer listType;

    @ApiModelProperty(value = "浏览量",example = "0",readOnly = true  )
    private Integer pv;

    @ApiModelProperty(value = "收藏数",example = "0",readOnly = true  )
    private Integer collectNum;

    @ApiModelProperty(value = "分享数",example = "0",readOnly = true  )
    private Integer shareNum;

    @ApiModelProperty(value = "评论数",example = "0",readOnly = true  )
    private Integer commentNum;

    @ApiModelProperty(value = "封面",example = ""  )
    private String cover;

    @ApiModelProperty(value = "单行文本",example = ""  )
    private String text;

    @Override
    public void save() {
       if(null==this.channelCode){
        this.channelCode="C01";
       }
        if(null==this.status){
        this.status=1==1;
        }
        if(null==this.isHot){
        this.isHot=0==1;
        }
        SysUserCacheInfo sysUserCacheInfo = JwtUtil.getSysUserCacheInfo();
        this.lbsId=sysUserCacheInfo.getLbsId();
        if(null==this.pv){
        this.pv=0;
        }
        if(null==this.collectNum){
        this.collectNum=0;
        }
        if(null==this.shareNum){
        this.shareNum=0;
        }
        if(null==this.commentNum){
        this.commentNum=0;
        }
        super.save();
    }


}
