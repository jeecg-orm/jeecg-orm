package com.hongru.cms.entity;

import com.hongru.ebean.HongRuEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.hongru.aspect.annotation.Dict;
/**
* @Description
* @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
* @Url https://www.xinhongru.com
* @ClassName LbsController
* @Author salter <salter@vip.163.com>
* @Version V1.0.0
* @Since 1.0
* @Date 2022/1/26 15:12
*/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Entity
@Table(name = "cms_member_question_problem")
@ApiModel("会员健康问卷问题记录表对象")
public class CmsMemberQuestionProblem extends HongRuEntity {


    @ApiModelProperty(value = "问卷名称",example = "中医体质问卷")
    private String questionName;

    @ApiModelProperty(value = "问题名称",example = "兴趣爱好")
    private String name;

    @ApiModelProperty(value = "问题回答",example = "跑步")
    private String content;

    @ApiModelProperty(value = "问卷ID",example = "fd3239c1a5c94a5b8ea29590596c88d1")
    private String questionId;

    @ApiModelProperty(value = "问题ID",example = "768248c96e1d4f3296d21c5a4d8f3c34")
    private String questionProblemId;

    @ApiModelProperty(value = "会员ID",example = "",readOnly = true)
    private String memberId;

    @Override
    public void save() {
       if(null==this.memberId){
        this.memberId="";
       }
        super.save();
    }


}
