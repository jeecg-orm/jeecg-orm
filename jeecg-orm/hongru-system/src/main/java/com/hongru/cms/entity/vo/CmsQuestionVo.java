package com.hongru.cms.entity.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hongru.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
* @Description
* @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
* @Url https://www.xinhongru.com
* @ClassName LbsController
* @Author salter <salter@vip.163.com>
* @Version V1.0.0
* @Since 1.0
* @Date 2022/1/26 15:12
*/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Entity
@Table(name = "cms_question")
@ApiModel("健康问卷对象")
public class CmsQuestionVo {

    @ApiModelProperty(hidden = true)
    private String id;

    @ApiModelProperty(value = "问卷名称",example = "中医体质问卷")
    private String name;

    @ApiModelProperty(value = "问卷简介",example = "中医医学问卷题：近一年疾病病史")
    private String intro;

    @ApiModelProperty(value = "排序",example = "")
    private Integer sort;

    @Transient
    @ApiModelProperty(value = "是否已填",example = "")
    private Boolean isCompleted;

    @JsonIgnore
    @ApiModelProperty(value = "问卷类型(2:中医体质问卷 1:整体医学问卷 )",example = "2")
    @Dict(dicCode = "question_type")
    private Integer questionType;
}
