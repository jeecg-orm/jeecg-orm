package com.hongru.cms.entity.vo;

import com.hongru.aspect.annotation.Dict;
import com.hongru.ebean.HongRuEntity;
import com.hongru.system.entity.SysCategory;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.*;

/**
* @Description
* @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
* @Url https://www.xinhongru.com
* @ClassName LbsController
* @Author salter <salter@vip.163.com>
* @Version V1.0.0
* @Since 1.0
* @Date 2022/1/26 15:12
*/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Entity
@Table(name = "cms_content")
@ApiModel("内容列表VO对象")
public class CmsContentListVo extends HongRuEntity {


    @ApiModelProperty(value = "栏目编码",example = "C01",readOnly = true)
    private String channelCode;

    @ApiModelProperty(value = "标题",example = "首页")
    private String title;

    @ApiModelProperty(value = "副标题",example = "0")
    private String subTitle;

    @ApiModelProperty(value = "简介",example = "0")
    private String intro;

    @ApiModelProperty(value = "图片",example = "0")
    private String image;

    @ApiModelProperty(value = "图片1",example = "0")
    private String image1;

    @ApiModelProperty(value = "文件",example = "0")
    private String file;

    @ApiModelProperty(value = "富文本",example = "0"  )
    private String detailCms;

    @ApiModelProperty(value = "跳转页面",example = "0")
    @Dict(dicCode = "id",dictTable ="sys_link",dicText = "url")
    private String linkId;

    @ApiModelProperty(value = "跳转数据ID",example = "0")
    private String dataId;

    @ApiModelProperty(value = "状态(0:关闭 1:开启 )",example = "0",readOnly = true)
    @Dict(dicCode = "status")
    private Boolean status;

    @ApiModelProperty(value = "分类",example = "F01")
    @Dict(dicCode = "code",dictTable ="sys_category",dicText = "name")
    private String categoryCode;

    @ApiModelProperty(value = "分类Icon",example = "")
    @Transient
    private String categoryIcon;

    @ApiModelProperty(value = "是否推荐(0:否 1:是 )",example = "0",readOnly = true)
    @Dict(dicCode = "is_open")
    private Boolean isHot;

    @ApiModelProperty(value = "项目ID",example = "",readOnly = true)
    private String lbsId;

    @ApiModelProperty(value = "列表展现形式(1:文字 2:图文 3:视频 )",example = "1")
    @Dict(dicCode = "content_list_type")
    private Integer listType;


    @ApiModelProperty(value = "封面",example = ""  )
    private String cover;

    @ApiModelProperty(value = "单行文本",example = ""  )
    private String text;


    @Transient
    @ApiModelProperty(value = "是否收藏(0:否 1:是 )",example = "0",readOnly = true)
    private Boolean isCollect;

    @Transient
    private String topImage;

}
