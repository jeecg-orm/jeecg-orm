package com.hongru.cms.entity.vo;

import com.hongru.aspect.annotation.Dict;
import com.hongru.ebean.HongRuEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
* @Description
* @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
* @Url https://www.xinhongru.com
* @ClassName LbsController
* @Author salter <salter@vip.163.com>
* @Version V1.0.0
* @Since 1.0
* @Date 2022/1/26 15:12
*/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Entity
@Table(name = "cms_video")
@ApiModel("运动视频VO对象")
public class CmsVideoVo extends HongRuEntity {


    @ApiModelProperty(value = "视频名称",example = "颈部康复")
    private String name;

    @ApiModelProperty(value = "视频类型",example = "F07")
    @Dict(dicCode = "code",dictTable ="sys_category",dicText = "name")
    private String categoryCode;

    @ApiModelProperty(value = "视频简介",example = "平躺在瑜伽垫上，双脚并拢，屈膝抬腿的同时将臀部略微抬起,下背部用力贴紧地面")
    private String intro;

    @ApiModelProperty(value = "消耗热量",example = "300")
    private String kcal;

    @ApiModelProperty(value = "视频封面",example = "")
    private String cover;

    @ApiModelProperty(value = "视频上传",example = "")
    private String path;

    @ApiModelProperty(value = "视频时长(秒)",example = ""  )
    private Integer duration;

    @Transient
    @ApiModelProperty(value = "是否完成打卡",example = "")
    private Boolean isClock;

}
