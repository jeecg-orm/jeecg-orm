package com.hongru.cms.entity;

import com.hongru.ebean.HongRuEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.hongru.aspect.annotation.Dict;
/**
* @Description
* @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
* @Url https://www.xinhongru.com
* @ClassName LbsController
* @Author salter <salter@vip.163.com>
* @Version V1.0.0
* @Since 1.0
* @Date 2022/1/26 15:12
*/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Entity
@Table(name = "cms_question")
@ApiModel("健康问卷对象")
public class CmsQuestion extends HongRuEntity {


    @ApiModelProperty(value = "问卷名称",example = "中医体质问卷",required = true  )
    private String name;

    @ApiModelProperty(value = "问卷简介",example = "中医医学问卷题：近一年疾病病史"  )
    private String intro;

    @ApiModelProperty(value = "是否启用(0:否 1:是 )",example = "0",readOnly = true )
    @Dict(dicCode = "is_open")
    private Boolean status;

    @ApiModelProperty(value = "问卷类型(1:整体医学问卷 2:中医体质问卷 )",example = "1" )
    @Dict(dicCode = "question_type")
    private Integer questionType;

    @ApiModelProperty(value = "题目数",example = "33"  )
    private Integer num;

    @ApiModelProperty(value = "填写时长",example = ""  )
    private String duration;

    @Override
    public void save() {
        if(null==this.status){
        this.status=1==1;
        }
        super.save();
    }


}
