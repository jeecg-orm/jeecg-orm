package com.hongru.ums.entity;

import com.hongru.ebean.HongRuEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.hongru.aspect.annotation.Dict;
/**
* @Description
* @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
* @Url https://www.xinhongru.com
* @ClassName LbsController
* @Author salter <salter@vip.163.com>
* @Version V1.0.0
* @Since 1.0
* @Date 2022/1/26 15:12
*/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Entity
@Table(name = "ums_exercise_plan_detail")
@ApiModel("运动计划明细对象")
public class UmsExercisePlanDetail extends HongRuEntity {


    @ApiModelProperty(value = "计划ID",example = "",readOnly = true  )
    private String exercisePlanId;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @ApiModelProperty(value = "计划时间",example = "")
    private Date planDate;

    @ApiModelProperty(value = "运动视频",example = "" )
    @Dict(dicCode = "id",dictTable ="cms_video",dicText = "name")
    private String videoIds;

    @ApiModelProperty(value = "是否打卡(0:否 1:是 )",example = "0",readOnly = true )
    @Dict(dicCode = "yn")
    private Integer status;

    @ApiModelProperty(value = "已打卡视频",example = "",readOnly = true  )
    private String clockVideoIds;

    @Override
    public void save() {
       if(null==this.exercisePlanId){
        this.exercisePlanId="";
       }
        if(null==this.status){
        this.status=0;
        }
       if(null==this.clockVideoIds){
        this.clockVideoIds="";
       }
        super.save();
    }


}
