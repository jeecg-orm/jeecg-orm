package com.hongru.ums.entity.vo;

import com.hongru.aspect.annotation.Dict;
import com.hongru.ebean.HongRuEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
* @Description
* @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
* @Url https://www.xinhongru.com
* @ClassName LbsController
* @Author salter <salter@vip.163.com>
* @Version V1.0.0
* @Since 1.0
* @Date 2022/1/26 15:12
*/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Entity
@Table(name = "ums_member")
@ApiModel("会员VO对象")
public class UmsMemberVo extends HongRuEntity {

    @ApiModelProperty(value = "是否授权昵称",example = "0",readOnly = true)
    private Boolean auth;

    @ApiModelProperty(value = "微信昵称",example = "salter")
    private String nickName;

    @ApiModelProperty(value = "微信头像",example = "https://portrait.gitee.com/uploads/avatars/user/2464/7394744_jeecg-salter_1648046300.png",readOnly = true)
    private String avatarUrl;

    @ApiModelProperty(value = "姓名",example = "刘晓天")
    private String realName;

    @ApiModelProperty(value = "手机号",example = "18813066492",readOnly = true)
    private String phone;

    @ApiModelProperty(value = "年龄",example = "18")
    private String age;

    @ApiModelProperty(value = "性别(0:未知 1:男 2:女 )",example = "0")
    @Dict(dicCode = "sex")
    private Integer gender;

    @ApiModelProperty(value = "身高",example = "192cm")
    private String height;

    @ApiModelProperty(value = "体重",example = "85kg")
    private String weight;

    @ApiModelProperty(value = "血型",example = "A")
    private String blood;

    @ApiModelProperty(value = "生日",example = "19940408",readOnly = true)
    private String birthday;

    @ApiModelProperty(value = "定制内容",example = "F06")
    @Dict(dicCode = "code",dictTable ="sys_category",dicText = "name")
    private String custom;

    @ApiModelProperty(value = "生活顾问",example = "")
    private String adviser;

    @ApiModelProperty(value = "健康诉求",example = "")
    private String appeal;

    @ApiModelProperty(value = "健康目标",example = "")
    private String target;

    @ApiModelProperty(value = "是否完成整体医学问卷(1:未完成 2:已完成 )",example = "1",readOnly = true )
    @Dict(dicCode = "completion_status")
    private Integer isZtyxQuestion;

    @ApiModelProperty(value = "是否完成中医体质问卷(1:未完成 2:已完成 )",example = "1",readOnly = true )
    @Dict(dicCode = "completion_status")
    private Integer isZytzQuestion;


    @ApiModelProperty(value = "整体医学建议检测",example = "F110" )
    @Dict(dicCode = "code",dictTable ="sys_category",dicText = "name")
    private String ztyxPe;

    @ApiModelProperty(value = "临床医学建议检测",example = "F111" )
    @Dict(dicCode = "code",dictTable ="sys_category",dicText = "name")
    private String lcyxPe;

    @ApiModelProperty(value = "定制会员(0:否 1:是 )", example = "0")
    @Dict(dicCode = "yn")
    private Integer svip;

    @Transient
    @ApiModelProperty(value = "未读消息数")
    private Integer msgCount;

    @Transient
    @ApiModelProperty(value = "未读消息数")
    private Integer collectCount;

    @Transient
    @ApiModelProperty(value = "token")
    private String token;

}
