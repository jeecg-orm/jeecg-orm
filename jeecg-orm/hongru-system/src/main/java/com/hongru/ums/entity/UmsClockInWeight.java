package com.hongru.ums.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hongru.ebean.HongRuEntity;
import com.hongru.util.DateUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * @Description
 * @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
 * @Url https://www.xinhongru.com
 * @ClassName LbsController
 * @Author salter <salter@vip.163.com>
 * @Version V1.0.0
 * @Since 1.0
 * @Date 2022/1/26 15:12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Entity
@Table(name = "ums_clock_in_weight")
@ApiModel("体重打卡对象")
public class UmsClockInWeight extends HongRuEntity {


    @ApiModelProperty(value = "体重", example = "72.5")
    private Double weight;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @ApiModelProperty(value = "打卡日期", example = "")
    private Date clockInDate;

    @ApiModelProperty(value = "会员ID", example = "", readOnly = true)
    private String memberId;

    @Override
    public void save() {
        if (null == this.memberId) {
            this.memberId = "";
        }
        super.save();
    }

}
