package com.hongru.ums.service;

import com.hongru.ums.entity.UmsPe;
import com.hongru.util.PDFToImgUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * @Description
 * @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
 * @Url https://www.xinhongru.com
 * @ClassName UmsPeService
 * @Author salter <salter@vip.163.com>
 * @Version V1.0.0
 * @Since 1.0
 * @Date 2022/6/15 16:24
 */
@Service
public class UmsPeService {
    @Value(value = "${hongru.path.upload}")
    private String uploadpath;
    @Async
    public void pdfToImage(UmsPe umsPe){
        String imageName = "/temp/" + umsPe.getName() + "_" + System.currentTimeMillis() + ".png";
        String imagePath  = uploadpath + imageName;
        PDFToImgUtil.pdfToOneImage(uploadpath+"/"+umsPe.getFile(),imagePath);
        umsPe.setImage(imageName);
        umsPe.update();
    }

}
