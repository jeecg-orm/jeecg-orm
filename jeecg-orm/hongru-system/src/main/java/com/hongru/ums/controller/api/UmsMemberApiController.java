package com.hongru.ums.controller.api;

import com.alibaba.fastjson.JSONObject;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.DynamicParameter;
import com.github.xiaoymin.knife4j.annotations.DynamicParameters;
import com.hongru.ebean.EbeanUtil;
import com.hongru.ebean.HongRuPage;
import com.hongru.ums.entity.UmsMember;
import com.hongru.vo.Result;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @Description
 * @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
 * @Url https://www.xinhongru.com
 * @ClassName UmsMemberApiController
 * @Author salter <salter@vip.163.com>
 * @Version V1.0.0
 * @Since 1.0
 * @Date 2022/6/9 9:51
 */
@RestController
@RequestMapping("/ums/member/admin")
@Slf4j
@ApiIgnore
public class UmsMemberApiController {

    @PostMapping("/list")
    public Result<HongRuPage<UmsMember>> queryMemberPageList(@RequestBody JSONObject searchObj) {
        searchObj.put("phone_notNull", searchObj.getString("phone"));
        return Result.OK(EbeanUtil.pageList(searchObj, UmsMember.class));
    }


}
