package com.hongru.ums.controller.api;


import com.alibaba.fastjson.JSONObject;
import com.hongru.ebean.EbeanUtil;
import com.hongru.ebean.HongRuPage;
import com.hongru.ums.entity.vo.UmsDynamicVo;
import com.hongru.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/ums/dynamic/admin")
@Slf4j
@ApiIgnore
public class UmsDynamicApiController {

    @PostMapping("/list")
    public Result<HongRuPage<UmsDynamicVo>> queryDynamicPageList(@RequestBody JSONObject searchObj) {
        return Result.OK(EbeanUtil.pageList(searchObj, UmsDynamicVo.class));
    }

}
