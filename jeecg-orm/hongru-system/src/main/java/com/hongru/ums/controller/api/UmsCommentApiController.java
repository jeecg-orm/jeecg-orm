package com.hongru.ums.controller.api;

import com.alibaba.fastjson.JSONObject;
import com.hongru.constant.DictConstant;
import com.hongru.ebean.EbeanUtil;
import com.hongru.ebean.HongRuPage;
import com.hongru.ums.entity.UmsComment;
import com.hongru.ums.entity.UmsMember;
import com.hongru.ums.entity.vo.UmsCommentVo;
import com.hongru.vo.LoginUser;
import com.hongru.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;


/**
 * @Description
 * @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
 * @Url https://www.xinhongru.com
 * @ClassName UmsCommentApiController
 * @Author salter <salter@vip.163.com>
 * @Version V1.0.0
 * @Since 1.0
 * @Date 2022/4/28 16:18
 */
@RestController
@RequestMapping("/api/ums/comment/")
@Slf4j
@ApiIgnore
public class UmsCommentApiController {

    @PostMapping("/list")
    public Result<HongRuPage<UmsCommentVo>> queryCommentPageList(@RequestBody JSONObject searchObj) {
        return Result.OK(EbeanUtil.pageList(searchObj, UmsCommentVo.class));
    }

    @PostMapping(value = "/add")
    public Result<UmsCommentVo> add(@RequestBody UmsComment umsComment) {
        LoginUser loginUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        List<UmsMember> memberList = EbeanUtil.initExpressionList(UmsMember.class).eq("userId", loginUser.getId()).findList();
        if (memberList.size() > 0) {
            umsComment.setMemberId(memberList.get(0).getId());
        }
        if (!umsComment.getType().equals(DictConstant.COMMENT_TYPE_UMSCOMMENT)) {
            umsComment.setTheme(umsComment.getType());
        } else {
            UmsCommentVo pComment = EbeanUtil.initExpressionList(UmsCommentVo.class).idEq(umsComment.getSourceId()).findOne();
            umsComment.setParentCommentMemberName(pComment.getMember().getNickName());
            umsComment.setTheme(pComment.getTheme());
        }
        umsComment.save();
        EbeanUtil.changeColumnNum(umsComment.getType(), "comment_num", umsComment.getSourceId(), "+1");
        return Result.OK("添加成功");
    }

}
