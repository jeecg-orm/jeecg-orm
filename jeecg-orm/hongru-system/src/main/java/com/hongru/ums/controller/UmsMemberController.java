package com.hongru.ums.controller;

import com.alibaba.fastjson.JSONObject;
import com.hongru.aspect.annotation.Log;
import com.hongru.ebean.StatusDto;
import com.hongru.ebean.SortNoDto;
import com.hongru.ebean.EbeanUtil;
import com.hongru.ebean.HongRuPage;
import com.hongru.ums.entity.UmsMember;
import com.hongru.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.DynamicParameter;
import com.github.xiaoymin.knife4j.annotations.DynamicParameters;

/**
* @Description
* @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
* @Url https://www.xinhongru.com
* @ClassName LbsController
* @Author salter <salter@vip.163.com>
* @Version V1.0.0
* @Since 1.0
* @Date 2022/1/26 15:12
*/
@RestController
@RequestMapping("/ums/member")
@Slf4j
@ApiIgnore
public class UmsMemberController {

    @PostMapping("/list")
    @ApiOperation("会员列表")
    @ApiOperationSupport(params = @DynamicParameters(properties = {
                @DynamicParameter(name = "isZtyxQuestion", value = "是否完成整体医学问卷(1:未完成 2:已完成 )",example = "1"),
                @DynamicParameter(name = "isZytzQuestion", value = "是否完成中医体质问卷(1:未完成 2:已完成 )",example = "1"),
                @DynamicParameter(name = "isComplete", value = "是否完善信息(0:否 1:是 )",example = "0"),
                @DynamicParameter(name = "mrStatus", value = "报告完成状态(0:未完成 1:进行中 2:已完成 )",example = "0"),
                @DynamicParameter(name = "svip", value = "定制会员(0:否 1:是 )",example = "0"),
    }))
    public Result<HongRuPage<UmsMember>> queryMemberPageList(@RequestBody JSONObject searchObj) {
        return Result.OK(EbeanUtil.pageList(searchObj, UmsMember.class));
    }

    @GetMapping("/{id}")
    @ApiOperation("详情")
    public Result<UmsMember> queryMemberById(@PathVariable String id) {
        UmsMember Member = EbeanUtil.initExpressionList(UmsMember.class).idEq(id).findOne();
        return Result.OK(Member);
    }

    @ApiOperation("添加")
    @ApiOperationSupport(ignoreParameters = {"umsMember.id"})
    @PostMapping(value = "/add")
    @Log(value = "会员-添加")
    public Result<UmsMember> add(@RequestBody UmsMember umsMember) {
        umsMember.save();
        return Result.OK("添加成功");
    }

    @PostMapping(value = "/edit")
    @Log(value = "会员-编辑")
    public Result<UmsMember> edit(@RequestBody UmsMember umsMember) {
        umsMember.update();
        return Result.OK("编辑成功");
    }

    @DeleteMapping(value = "/delete")
    @Log(value = "会员-删除")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        EbeanUtil.delete(id, UmsMember.class);
        return Result.OK("删除成功");
    }
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        EbeanUtil.deleteBatch(ids, UmsMember.class);
        return Result.OK("批量删除成功");
    }

    @PostMapping(value="/field/{field}")
    public Result<?> field(@PathVariable String field, @RequestBody StatusDto statusDto) {
        EbeanUtil.field(field,statusDto,UmsMember.class);
        return Result.OK();
    }

    @PostMapping(value="/sortNo")
    public Result<?> sortNo(@RequestBody SortNoDto sortNoDto) {
        EbeanUtil.sortNo(sortNoDto,UmsMember.class);
        return Result.OK();
    }





}

