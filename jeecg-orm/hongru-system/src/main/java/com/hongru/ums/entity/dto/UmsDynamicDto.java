package com.hongru.ums.entity.dto;

import com.hongru.aspect.annotation.Dict;
import com.hongru.ebean.HongRuEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Description
 * @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
 * @Url https://www.xinhongru.com
 * @ClassName LbsController
 * @Author salter <salter@vip.163.com>
 * @Version V1.0.0
 * @Since 1.0
 * @Date 2022/1/26 15:12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel("动态消息Dto对象")
public class UmsDynamicDto extends HongRuEntity {

    @ApiModelProperty(value = "消息类型(txt:文本 img:图片 video:视频 )", example = "txt", required = true)
    @Dict(dicCode = "dynamic_type")
    private String type;

    @ApiModelProperty(value = "文件路径,图片类型逗号拼接", example = "")
    private String filePath;

    @ApiModelProperty(value = "话题(1:日常动态 2:饮食打卡 )", example = "1", required = true)
    @Dict(dicCode = "topic")
    @NotNull(message = "话题不能为空")
    private Integer topic;

    @ApiModelProperty(value = "文字内容", example = "")
    private String messageText;

    @ApiModelProperty(value = "是否同步健康圈(1:是 0:否 )", example = "1")
    @Dict(dicCode = "is_open")
    private Integer isShow;

}
