package com.hongru.ums.entity;

import com.hongru.aspect.annotation.Dict;
import com.hongru.ebean.HongRuEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.Table;
/**
* @Description
* @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
* @Url https://www.xinhongru.com
* @ClassName LbsController
* @Author salter <salter@vip.163.com>
* @Version V1.0.0
* @Since 1.0
* @Date 2022/1/26 15:12
*/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Entity
@Table(name = "ums_clock_in_template")
@ApiModel("打卡模板对象")
public class UmsClockInTemplate extends HongRuEntity {


    @ApiModelProperty(value = "模板名称",example = "")
    private String name;

    @ApiModelProperty(value = "主标题",example = "")
    private String title;

    @ApiModelProperty(value = "正文",example = "")
    private String content;

    @ApiModelProperty(value = "背景图",example = "")
    private String cover;

    @ApiModelProperty(value = "状态(0:否 1:是 )",example = "0",readOnly = true)
    @Dict(dicCode = "is_open")
    private Boolean status;

    @ApiModelProperty(value = "模板类型(m1:体重模板 m2:运动模板 )",example = "m1")
    @Dict(dicCode = "clockIn_template_type")
    private String type;

    @Override
    public void save() {
        if(null==this.status){
        this.status=0==1;
        }
        super.save();
    }


}
