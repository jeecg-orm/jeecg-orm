package com.hongru.ums.entity.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hongru.aspect.annotation.Dict;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @Description
 * @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
 * @Url https://www.xinhongru.com
 * @ClassName UmsPeUpload
 * @Author salter <salter@vip.163.com>
 * @Version V1.0.0
 * @Since 1.0
 * @Date 2022/4/20 16:38
 */
@Data
public class UmsPeUpload {

    @ApiModelProperty(value = "报告名称",example = " 2022年10月份身体成分检测")
    private String name;

    @ApiModelProperty(value = "检测项目",example = "F05")
    private String categoryCode;

    @ApiModelProperty(value = "图片",example = "",readOnly = true)
    private String image;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @ApiModelProperty(value = "检测日期",example = "")
    private Date peDate;

}
