package com.hongru.ums.entity.vo;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.hongru.aspect.annotation.Dict;
import com.hongru.ebean.HongRuEntity;
import com.hongru.ums.entity.UmsExercisePlan;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * @Description
 * @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
 * @Url https://www.xinhongru.com
 * @ClassName LbsController
 * @Author salter <salter@vip.163.com>
 * @Version V1.0.0
 * @Since 1.0
 * @Date 2022/1/26 15:12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Entity
@Table(name = "ums_exercise_plan_detail")
@ApiModel("运动计划明细对象")
public class UmsExercisePlanDetailVo extends HongRuEntity {
    @ManyToOne
    @JoinColumn(name = "exercise_plan_id")
    @JsonBackReference
    private UmsExercisePlan exercisePlan;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @ApiModelProperty(value = "计划时间", example = "")
    private Date planDate;

    @ApiModelProperty(value = "运动视频", example = "")
    @Dict(dicCode = "id", dictTable = "cms_video", dicText = "name")
    private String videoIds;

    @ApiModelProperty(value = "是否打卡", example = "0", readOnly = true)
    private Boolean status;

}
