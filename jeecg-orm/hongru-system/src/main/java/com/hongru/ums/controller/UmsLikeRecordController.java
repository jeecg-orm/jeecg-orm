package com.hongru.ums.controller;

import com.alibaba.fastjson.JSONObject;
import com.hongru.aspect.annotation.Log;
import com.hongru.ebean.StatusDto;
import com.hongru.ebean.SortNoDto;
import com.hongru.ebean.EbeanUtil;
import com.hongru.ebean.HongRuPage;
import com.hongru.ums.entity.UmsLikeRecord;
import com.hongru.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.DynamicParameter;
import com.github.xiaoymin.knife4j.annotations.DynamicParameters;

/**
* @Description
* @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
* @Url https://www.xinhongru.com
* @ClassName LbsController
* @Author salter <salter@vip.163.com>
* @Version V1.0.0
* @Since 1.0
* @Date 2022/1/26 15:12
*/
@RestController
@RequestMapping("/ums/like/record")
@Slf4j
@ApiIgnore
public class UmsLikeRecordController {

    @PostMapping("/list")
    @ApiOperation("点赞收藏记录列表")
    @ApiOperationSupport(params = @DynamicParameters(properties = {
    }))
    public Result<HongRuPage<UmsLikeRecord>> queryLikeRecordPageList(@RequestBody JSONObject searchObj) {
        return Result.OK(EbeanUtil.pageList(searchObj, UmsLikeRecord.class));
    }

    @GetMapping("/{id}")
    @ApiOperation("详情")
    public Result<UmsLikeRecord> queryLikeRecordById(@PathVariable String id) {
        UmsLikeRecord LikeRecord = EbeanUtil.initExpressionList(UmsLikeRecord.class).idEq(id).findOne();
        return Result.OK(LikeRecord);
    }

    @ApiOperation("添加")
    @ApiOperationSupport(ignoreParameters = {"umsLikeRecord.id"})
    @PostMapping(value = "/add")
    @Log(value = "点赞收藏记录-添加")
    public Result<UmsLikeRecord> add(@RequestBody UmsLikeRecord umsLikeRecord) {
        umsLikeRecord.save();
        return Result.OK("添加成功");
    }

    @PostMapping(value = "/edit")
    @Log(value = "点赞收藏记录-编辑")
    public Result<UmsLikeRecord> edit(@RequestBody UmsLikeRecord umsLikeRecord) {
        umsLikeRecord.update();
        return Result.OK("编辑成功");
    }

    @DeleteMapping(value = "/delete")
    @Log(value = "点赞收藏记录-删除")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        EbeanUtil.delete(id, UmsLikeRecord.class);
        return Result.OK("删除成功");
    }
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        EbeanUtil.deleteBatch(ids, UmsLikeRecord.class);
        return Result.OK("批量删除成功");
    }

    @PostMapping(value="/field/{field}")
    public Result<?> field(@PathVariable String field, @RequestBody StatusDto statusDto) {
        EbeanUtil.field(field,statusDto,UmsLikeRecord.class);
        return Result.OK();
    }

    @PostMapping(value="/sortNo")
    public Result<?> sortNo(@RequestBody SortNoDto sortNoDto) {
        EbeanUtil.sortNo(sortNoDto,UmsLikeRecord.class);
        return Result.OK();
    }





}

