package com.hongru.ums.entity.vo;

import com.hongru.aspect.annotation.Dict;
import com.hongru.ebean.HongRuEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
* @Description
* @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
* @Url https://www.xinhongru.com
* @ClassName LbsController
* @Author salter <salter@vip.163.com>
* @Version V1.0.0
* @Since 1.0
* @Date 2022/1/26 15:12
*/
@Data
public class UmsClockInTemplateVo {

    @ApiModelProperty(value = "模板名称",example = "")
    private String name;

    @ApiModelProperty(value = "主标题",example = "")
    private String title;

    @ApiModelProperty(value = "正文",example = "")
    private String content;

    @ApiModelProperty(value = "背景图",example = "")
    private String cover;

    @ApiModelProperty(value = "起始体重",example = "80")
    private Double oldWeight;

    @ApiModelProperty(value = "目前体重",example = "75")
    private Double newWeight;

}
