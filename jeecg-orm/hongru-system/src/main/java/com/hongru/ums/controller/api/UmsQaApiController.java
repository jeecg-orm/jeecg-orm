package com.hongru.ums.controller.api;

import com.alibaba.fastjson.JSONObject;
import com.hongru.aspect.annotation.Log;
import com.hongru.constant.DictConstant;
import com.hongru.ebean.EbeanUtil;
import com.hongru.ebean.HongRuPage;
import com.hongru.ums.entity.UmsQa;
import com.hongru.ums.entity.vo.UmsQaVo;
import com.hongru.vo.Result;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/ums/qa/admin")
@Slf4j
@ApiIgnore
public class UmsQaApiController {

    @PostMapping("/list")
    @ApiOperation("健康问答列表")
    public Result<HongRuPage<UmsQaVo>> queryQaPageList(@RequestBody JSONObject searchObj) {
        return Result.OK(EbeanUtil.pageList(searchObj, UmsQaVo.class));
    }


    @PostMapping(value = "/edit")
    @Log(value = "健康问答-编辑")
    public Result<UmsQa> edit(@RequestBody UmsQa umsQa) {
        umsQa.setIsReply(DictConstant.IS_OPEN_1);
        umsQa.update();
        return Result.OK("编辑成功");
    }

}
