package com.hongru.ums.controller;

import com.alibaba.fastjson.JSONObject;
import com.hongru.aspect.annotation.Log;
import com.hongru.ebean.StatusDto;
import com.hongru.ebean.SortNoDto;
import com.hongru.ebean.EbeanUtil;
import com.hongru.ebean.HongRuPage;
import com.hongru.ums.entity.UmsMemberLoginLog;
import com.hongru.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.DynamicParameter;
import com.github.xiaoymin.knife4j.annotations.DynamicParameters;

/**
* @Description
* @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
* @Url https://www.xinhongru.com
* @ClassName LbsController
* @Author salter <salter@vip.163.com>
* @Version V1.0.0
* @Since 1.0
* @Date 2022/1/26 15:12
*/
@RestController
@RequestMapping("/ums/member/login/log")
@Slf4j
@ApiIgnore
public class UmsMemberLoginLogController {

    @PostMapping("/list")
    @ApiOperation("会员登录日志列表")
    @ApiOperationSupport(params = @DynamicParameters(properties = {
    }))
    public Result<HongRuPage<UmsMemberLoginLog>> queryMemberLoginLogPageList(@RequestBody JSONObject searchObj) {
        return Result.OK(EbeanUtil.pageList(searchObj, UmsMemberLoginLog.class));
    }

    @GetMapping("/{id}")
    @ApiOperation("详情")
    public Result<UmsMemberLoginLog> queryMemberLoginLogById(@PathVariable String id) {
        UmsMemberLoginLog MemberLoginLog = EbeanUtil.initExpressionList(UmsMemberLoginLog.class).idEq(id).findOne();
        return Result.OK(MemberLoginLog);
    }

    @ApiOperation("添加")
    @ApiOperationSupport(ignoreParameters = {"umsMemberLoginLog.id"})
    @PostMapping(value = "/add")
    @Log(value = "会员登录日志-添加")
    public Result<UmsMemberLoginLog> add(@RequestBody UmsMemberLoginLog umsMemberLoginLog) {
        umsMemberLoginLog.save();
        return Result.OK("添加成功");
    }

    @PostMapping(value = "/edit")
    @Log(value = "会员登录日志-编辑")
    public Result<UmsMemberLoginLog> edit(@RequestBody UmsMemberLoginLog umsMemberLoginLog) {
        umsMemberLoginLog.update();
        return Result.OK("编辑成功");
    }

    @DeleteMapping(value = "/delete")
    @Log(value = "会员登录日志-删除")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        EbeanUtil.delete(id, UmsMemberLoginLog.class);
        return Result.OK("删除成功");
    }
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        EbeanUtil.deleteBatch(ids, UmsMemberLoginLog.class);
        return Result.OK("批量删除成功");
    }

    @PostMapping(value="/field/{field}")
    public Result<?> field(@PathVariable String field, @RequestBody StatusDto statusDto) {
        EbeanUtil.field(field,statusDto,UmsMemberLoginLog.class);
        return Result.OK();
    }

    @PostMapping(value="/sortNo")
    public Result<?> sortNo(@RequestBody SortNoDto sortNoDto) {
        EbeanUtil.sortNo(sortNoDto,UmsMemberLoginLog.class);
        return Result.OK();
    }





}

