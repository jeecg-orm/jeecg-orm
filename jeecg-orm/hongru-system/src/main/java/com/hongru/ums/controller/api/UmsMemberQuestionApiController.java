package com.hongru.ums.controller.api;

import com.hongru.cms.entity.CmsMemberQuestionProblem;
import com.hongru.ebean.EbeanUtil;
import com.hongru.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Description
 * @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
 * @Url https://www.xinhongru.com
 * @ClassName UmsMemberQuestionApiController
 * @Author salter <salter@vip.163.com>
 * @Version V1.0.0
 * @Since 1.0
 * @Date 2022/4/26 18:14
 */
@RestController
@RequestMapping("/ums/member/question")
@Slf4j
public class UmsMemberQuestionApiController {

    @GetMapping("/{memberId}")
    public Result list(@PathVariable String memberId) {
        List<CmsMemberQuestionProblem> memberQuestionProblemList = EbeanUtil.initExpressionList(CmsMemberQuestionProblem.class).eq("memberId", memberId).findList();
        if (memberQuestionProblemList.size() == 0) {
            return Result.error("暂无数据");
        }
        Map<String, List<CmsMemberQuestionProblem>> memberQuestionProblemGroupMap = memberQuestionProblemList.stream().collect(Collectors.groupingBy(CmsMemberQuestionProblem::getQuestionName));
        return Result.OK(memberQuestionProblemGroupMap);
    }

}
