package com.hongru.ums.entity;

import com.hongru.ebean.HongRuEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import javax.persistence.Entity;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.hongru.aspect.annotation.Dict;
/**
* @Description
* @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
* @Url https://www.xinhongru.com
* @ClassName LbsController
* @Author salter <salter@vip.163.com>
* @Version V1.0.0
* @Since 1.0
* @Date 2022/1/26 15:12
*/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Entity
@ApiModel("运动康复计划对象")
public class UmsExercisePlan extends HongRuEntity {


    @ApiModelProperty(value = "计划名称",example = "2022年10月份减脂计划第一阶段")
    private String name;

    @ApiModelProperty(value = "状态(0:否 1:是 )",example = "0",readOnly = true)
    @Dict(dicCode = "is_open")
    private Boolean status;

    @ApiModelProperty(value = "会员ID",example = "",readOnly = true)
    private String memberId;

    @Override
    public void save() {
        if(null==this.status){
        this.status=0==1;
        }
        if(null==this.memberId){
        this.memberId="";
        }
        super.save();
    }


}
