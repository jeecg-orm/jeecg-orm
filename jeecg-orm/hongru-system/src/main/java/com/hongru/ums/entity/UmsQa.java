package com.hongru.ums.entity;

import com.hongru.ebean.HongRuEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.hongru.aspect.annotation.Dict;
/**
* @Description
* @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
* @Url https://www.xinhongru.com
* @ClassName LbsController
* @Author salter <salter@vip.163.com>
* @Version V1.0.0
* @Since 1.0
* @Date 2022/1/26 15:12
*/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Entity
@Table(name = "ums_qa")
@ApiModel("健康问答对象")
public class UmsQa extends HongRuEntity {


    @ApiModelProperty(value = "会员ID",example = "",readOnly = true)
    private String memberId;

    @ApiModelProperty(value = "问题",example = "")
    private String question;

    @ApiModelProperty(value = "回复内容",example = "")
    private String answer;

    @ApiModelProperty(value = "是否回复(0:否 1:是 )",example = "0",readOnly = true)
    @Dict(dicCode = "is_open")
    private Integer isReply;

    @ApiModelProperty(value = "点赞数",example = "0",readOnly = true)
    private Integer likeNum;

    @ApiModelProperty(value = "浏览量",example = "0",readOnly = true)
    private Integer pv;

    @Override
    public void save() {
       if(null==this.memberId){
        this.memberId="";
       }
        if(null==this.isReply){
        this.isReply=0;
        }
        if(null==this.likeNum){
        this.likeNum=0;
        }
        if(null==this.pv){
        this.pv=0;
        }
        super.save();
    }


}
