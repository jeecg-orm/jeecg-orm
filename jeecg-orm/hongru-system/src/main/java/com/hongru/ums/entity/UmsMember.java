package com.hongru.ums.entity;

import com.hongru.ebean.HongRuEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.hongru.aspect.annotation.Dict;

/**
 * @Description
 * @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
 * @Url https://www.xinhongru.com
 * @ClassName LbsController
 * @Author salter <salter@vip.163.com>
 * @Version V1.0.0
 * @Since 1.0
 * @Date 2022/1/26 15:12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Entity
@Table(name = "ums_member")
@ApiModel("会员对象")
public class UmsMember extends HongRuEntity {


    @ApiModelProperty(value = "open_id", example = "", readOnly = true)
    private String openId;

    @ApiModelProperty(value = "union_id", example = "", readOnly = true)
    private String unionId;

    @ApiModelProperty(value = "是否授权昵称", example = "0", readOnly = true)
    private Boolean auth;

    @ApiModelProperty(value = "微信昵称", example = "salter")
    private String nickName;

    @ApiModelProperty(value = "微信头像", example = "https://jo.jeecgorm.com/jeecg-orm/images/avaurl.jpg", readOnly = true)
    private String avatarUrl;

    @ApiModelProperty(value = "姓名", example = "刘晓天")
    private String realName;

    @ApiModelProperty(value = "手机号", example = "18813066492")
    private String phone;

    @ApiModelProperty(value = "年龄", example = "18")
    private String age;

    @ApiModelProperty(value = "性别(0:未知 1:男 2:女 )", example = "0")
    @Dict(dicCode = "sex")
    private Integer gender;

    @ApiModelProperty(value = "身高", example = "192cm")
    private String height;

    @ApiModelProperty(value = "体重", example = "85kg")
    private String weight;

    @ApiModelProperty(value = "血型", example = "A")
    private String blood;

    @ApiModelProperty(value = "生日", example = "", readOnly = true)
    private String birthday;

    @ApiModelProperty(value = "定制内容", example = "F06")
    @Dict(dicCode = "code", dictTable = "sys_category", dicText = "name")
    private String custom;

    @ApiModelProperty(value = "健康顾问", example = "")
    private String adviser;

    @ApiModelProperty(value = "健康诉求", example = "")
    private String appeal;

    @ApiModelProperty(value = "健康目标", example = "")
    private String target;

    @ApiModelProperty(value = "会员类型(0:会员 1:虚拟会员 )", example = "0", readOnly = true, required = true)
    @Dict(dicCode = "member_type")
    private Integer memberType;

    @ApiModelProperty(value = "是否完成整体医学问卷(1:未完成 2:已完成 )", example = "1", readOnly = true)
    @Dict(dicCode = "completion_status")
    private Integer isZtyxQuestion;

    @ApiModelProperty(value = "是否完成中医体质问卷(1:未完成 2:已完成 )", example = "1", readOnly = true)
    @Dict(dicCode = "completion_status")
    private Integer isZytzQuestion;

    @ApiModelProperty(value = "是否完善信息(0:否 1:是 )", example = "0", readOnly = true)
    @Dict(dicCode = "yn")
    private Integer isComplete;

    @ApiModelProperty(value = "报告完成状态(0:未完成 1:进行中 2:已完成 )", example = "0", readOnly = true)
    @Dict(dicCode = "mr_status")
    private Integer mrStatus;

    @ApiModelProperty(value = "定制会员(0:否 1:是 )", example = "0")
    @Dict(dicCode = "yn")
    private Integer svip;

    @ApiModelProperty(value = "已完成报告", example = "", readOnly = true)
    private String mrText;

    @ApiModelProperty(value = "管理员ID", example = "", readOnly = true)
    private String userId;

    @ApiModelProperty(value = "常用地址", example = "", readOnly = true)
    private String address;

    @ApiModelProperty(value = "整体医学建议检测", example = "F110")
    @Dict(dicCode = "code", dictTable = "sys_category", dicText = "name")
    private String ztyxPe;

    @ApiModelProperty(value = "临床医学建议检测", example = "F111")
    @Dict(dicCode = "code", dictTable = "sys_category", dicText = "name")
    private String lcyxPe;

    @Override
    public void save() {
        if (null == this.openId) {
            this.openId = "";
        }
        if (null == this.unionId) {
            this.unionId = "";
        }
        if (null == this.auth) {
            this.auth = 0 == 1;
        }
        if (null == this.avatarUrl) {
            this.avatarUrl = "https://bixu.jeecgorm.com/jeecg-orm/images/avaurl.jpg";
        }
        if (null == this.birthday) {
            this.birthday = "";
        }
        if (null == this.memberType) {
            this.memberType = 0;
        }
        if (null == this.gender) {
            this.gender = 0;
        }
        if (null == this.isZtyxQuestion) {
            this.isZtyxQuestion = 1;
        }
        if (null == this.isZytzQuestion) {
            this.isZytzQuestion = 1;
        }
        if (null == this.isComplete) {
            this.isComplete = 0;
        }
        if (null == this.mrStatus) {
            this.mrStatus = 0;
        }
        if (null == this.mrText) {
            this.mrText = "";
        }
        if (null == this.userId) {
            this.userId = "";
        }
        if (null == this.address) {
            this.address = "";
        }
        super.save();
    }


}
