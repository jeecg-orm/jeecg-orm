package com.hongru.ums.controller;

import com.alibaba.fastjson.JSONObject;
import com.hongru.aspect.annotation.Log;
import com.hongru.ebean.StatusDto;
import com.hongru.ebean.SortNoDto;
import com.hongru.ebean.EbeanUtil;
import com.hongru.ebean.HongRuPage;
import com.hongru.ums.entity.UmsQa;
import com.hongru.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.DynamicParameter;
import com.github.xiaoymin.knife4j.annotations.DynamicParameters;

/**
* @Description
* @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
* @Url https://www.xinhongru.com
* @ClassName LbsController
* @Author salter <salter@vip.163.com>
* @Version V1.0.0
* @Since 1.0
* @Date 2022/1/26 15:12
*/
@RestController
@RequestMapping("/ums/qa")
@Slf4j
@ApiIgnore
public class UmsQaController {

    @PostMapping("/list")
    @ApiOperation("健康问答列表")
    @ApiOperationSupport(params = @DynamicParameters(properties = {
                @DynamicParameter(name = "createTime", value = "提问日期",example = ""),
                @DynamicParameter(name = "question", value = "问题",example = ""),
                @DynamicParameter(name = "isReply", value = "是否回复(0:否 1:是 )",example = "0"),
                @DynamicParameter(name = "member_nickName", value = "会员昵称",example = ""),
    }))
    public Result<HongRuPage<UmsQa>> queryQaPageList(@RequestBody JSONObject searchObj) {
        return Result.OK(EbeanUtil.pageList(searchObj, UmsQa.class));
    }

    @GetMapping("/{id}")
    @ApiOperation("详情")
    public Result<UmsQa> queryQaById(@PathVariable String id) {
        UmsQa Qa = EbeanUtil.initExpressionList(UmsQa.class).idEq(id).findOne();
        return Result.OK(Qa);
    }

    @ApiOperation("添加")
    @ApiOperationSupport(ignoreParameters = {"umsQa.id"})
    @PostMapping(value = "/add")
    @Log(value = "健康问答-添加")
    public Result<UmsQa> add(@RequestBody UmsQa umsQa) {
        umsQa.save();
        return Result.OK("添加成功");
    }

    @PostMapping(value = "/edit")
    @Log(value = "健康问答-编辑")
    public Result<UmsQa> edit(@RequestBody UmsQa umsQa) {
        umsQa.update();
        return Result.OK("编辑成功");
    }

    @DeleteMapping(value = "/delete")
    @Log(value = "健康问答-删除")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        EbeanUtil.delete(id, UmsQa.class);
        return Result.OK("删除成功");
    }
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        EbeanUtil.deleteBatch(ids, UmsQa.class);
        return Result.OK("批量删除成功");
    }

    @PostMapping(value="/field/{field}")
    public Result<?> field(@PathVariable String field, @RequestBody StatusDto statusDto) {
        EbeanUtil.field(field,statusDto,UmsQa.class);
        return Result.OK();
    }

    @PostMapping(value="/sortNo")
    public Result<?> sortNo(@RequestBody SortNoDto sortNoDto) {
        EbeanUtil.sortNo(sortNoDto,UmsQa.class);
        return Result.OK();
    }





}

