package com.hongru.ums.controller.api;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.hongru.constant.DictConstant;
import com.hongru.ebean.EbeanUtil;
import com.hongru.system.entity.SysCategory;
import com.hongru.ums.entity.UmsMember;
import com.hongru.ums.entity.UmsMr;
import com.hongru.util.StringUtil;
import com.hongru.vo.Result;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/ums/mr/admin")
@Slf4j
@ApiIgnore
public class UmsMrApiController {

    @ApiOperation("添加")
    @ApiOperationSupport(ignoreParameters = {"umsMr.id"})
    @PostMapping(value = "/add")
    public Result<UmsMr> add(@RequestBody UmsMr umsMr) {
        umsMr.save();
        SysCategory category = EbeanUtil.initExpressionList(SysCategory.class).eq("code", umsMr.getCategoryCode()).findOne();
        UmsMember member = EbeanUtil.initExpressionList(UmsMember.class).idIn(umsMr.getMemberId()).findOne();
        if (StringUtil.isEmpty(member.getMrText())) {
            member.setMrText(category.getName());
        }
        if (!StringUtil.isEmpty(member.getMrText()) && !member.getMrText().contains(category.getName())) {
            member.setMrText(member.getMrText() + "," + category.getName());
        }
        member.setMrStatus(DictConstant.MR_STATUS_1);
        member.update();
        return Result.OK("添加成功");
    }

}
