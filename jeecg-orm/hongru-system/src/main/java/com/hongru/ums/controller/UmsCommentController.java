package com.hongru.ums.controller;

import com.alibaba.fastjson.JSONObject;
import com.hongru.aspect.annotation.Log;
import com.hongru.ebean.StatusDto;
import com.hongru.ebean.SortNoDto;
import com.hongru.ebean.EbeanUtil;
import com.hongru.ebean.HongRuPage;
import com.hongru.ums.entity.UmsComment;
import com.hongru.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.DynamicParameter;
import com.github.xiaoymin.knife4j.annotations.DynamicParameters;

/**
* @Description
* @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
* @Url https://www.xinhongru.com
* @ClassName LbsController
* @Author salter <salter@vip.163.com>
* @Version V1.0.0
* @Since 1.0
* @Date 2022/1/26 15:12
*/
@RestController
@RequestMapping("/ums/comment")
@Slf4j
@ApiIgnore
public class UmsCommentController {

    @PostMapping("/list")
    @ApiOperation("评论列表")
    @ApiOperationSupport(params = @DynamicParameters(properties = {
                @DynamicParameter(name = "memberId", value = "会员ID",example = ""),
                @DynamicParameter(name = "sourceId", value = "评论数据ID",example = ""),
                @DynamicParameter(name = "type", value = "评论类型(UmsDynamic:动态 CmsContent:内容 UmsComment:评论 )",example = "UmsDynamic"),
                @DynamicParameter(name = "status", value = "状态(0:关闭 1:开启 )",example = "0"),
    }))
    public Result<HongRuPage<UmsComment>> queryCommentPageList(@RequestBody JSONObject searchObj) {
        return Result.OK(EbeanUtil.pageList(searchObj, UmsComment.class));
    }

    @GetMapping("/{id}")
    @ApiOperation("详情")
    public Result<UmsComment> queryCommentById(@PathVariable String id) {
        UmsComment Comment = EbeanUtil.initExpressionList(UmsComment.class).idEq(id).findOne();
        return Result.OK(Comment);
    }

    @ApiOperation("添加")
    @ApiOperationSupport(ignoreParameters = {"umsComment.id"})
    @PostMapping(value = "/add")
    @Log(value = "评论-添加")
    public Result<UmsComment> add(@RequestBody UmsComment umsComment) {
        umsComment.save();
        return Result.OK("添加成功");
    }

    @PostMapping(value = "/edit")
    @Log(value = "评论-编辑")
    public Result<UmsComment> edit(@RequestBody UmsComment umsComment) {
        umsComment.update();
        return Result.OK("编辑成功");
    }

    @DeleteMapping(value = "/delete")
    @Log(value = "评论-删除")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        EbeanUtil.delete(id, UmsComment.class);
        return Result.OK("删除成功");
    }
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        EbeanUtil.deleteBatch(ids, UmsComment.class);
        return Result.OK("批量删除成功");
    }

    @PostMapping(value="/field/{field}")
    public Result<?> field(@PathVariable String field, @RequestBody StatusDto statusDto) {
        EbeanUtil.field(field,statusDto,UmsComment.class);
        return Result.OK();
    }

    @PostMapping(value="/sortNo")
    public Result<?> sortNo(@RequestBody SortNoDto sortNoDto) {
        EbeanUtil.sortNo(sortNoDto,UmsComment.class);
        return Result.OK();
    }





}

