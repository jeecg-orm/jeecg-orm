package com.hongru.constant;

/**
* @Description
* @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
* @Url https://www.xinhongru.com
* @ClassName DictConstant
* @Author salter <salter@vip.163.com>
* @Version V1.0.0
* @Since 1.0
* @Date 2022/4/22 10:01
*/
public interface DictConstant {

<#if dictList??>
    <#list dictList as dict>
        /**
        * ${dict.dictName}
        */
        <#list dict.dictItems as dictItem>
            <#if dictItem.itemValue?matches("-?[1-9]\\d*|0")>
        public static final Integer ${dict.dictCode?upper_case}_${dictItem.itemValue?upper_case} = ${dictItem.itemValue};//${dictItem.itemText}
                <#else>
        public static final String ${dict.dictCode?upper_case}_${dictItem.itemValue?upper_case} = "${dictItem.itemValue}";//${dictItem.itemText}
            </#if>
        </#list>
    </#list>
</#if>

}
