package com.hongru.exception;

public class JeecgOrmException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public JeecgOrmException(String message){
		super(message);
	}

	public JeecgOrmException(Throwable cause)
	{
		super(cause);
	}

	public JeecgOrmException(String message, Throwable cause)
	{
		super(message,cause);
	}
}
