package com.hongru.vo;


import lombok.Data;


@Data
public class SysUserCacheInfo {

	private String id;
	private String lbsId;
	private String username;

}
