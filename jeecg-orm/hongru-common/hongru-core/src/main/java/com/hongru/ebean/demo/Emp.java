package com.hongru.ebean.demo;


import com.hongru.ebean.HongRuEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "demo_emp")
@Data
public class Emp extends HongRuEntity {
    String name;
    String no;

    @ManyToOne
    Dept dept;

}
