package com.hongru.aspect.annotation;

import java.lang.annotation.*;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Log {
    /**
     * 添加或删除时，需要显示的字段名 例如 内容-添加 标题：xxx
     */
    String name() default "name";
    String value();
    String className() default "";
}
