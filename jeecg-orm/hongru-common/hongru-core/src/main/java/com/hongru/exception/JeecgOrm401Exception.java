package com.hongru.exception;

public class JeecgOrm401Exception extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public JeecgOrm401Exception(String message){
		super(message);
	}

	public JeecgOrm401Exception(Throwable cause)
	{
		super(cause);
	}

	public JeecgOrm401Exception(String message, Throwable cause)
	{
		super(message,cause);
	}
}
