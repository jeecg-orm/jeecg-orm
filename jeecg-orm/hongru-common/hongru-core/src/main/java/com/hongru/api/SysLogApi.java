package com.hongru.api;

import com.hongru.aspect.annotation.Log;
import org.aspectj.lang.JoinPoint;

/**
 * @Description
 * @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
 * @Url https://www.xinhongru.com
 * @ClassName SysLogApi
 * @Author salter <salter@vip.163.com>
 * @Version V1.0.0
 * @Since 1.0
 * @Date 2022/4/25 11:02
 */
public interface SysLogApi {
    void putUpdate(JoinPoint joinPoint, String methodName, String name, String content);
    void putSave(JoinPoint joinPoint, String methodName, String name, String content);
    void putDelete(JoinPoint joinPoint, String methodName, String name, String content,String className);
}
