package com.hongru.ebean.demo;

import io.ebean.*;
import java.util.List;

public class EbeanDemo {

    public static void main(String[] args) {

    }

    /**
     *
     * 级联保存部门，及员工
     */
    static void save() {
        Dept dept = new Dept();
        dept.setName("技术部");
        dept.setNo("ruoya-tech");
        for(int i=0;i<2;i++){
            Emp emp =new Emp();
            emp.setName("tech");
            emp.setNo("tech");
            emp.setDept(dept);
            dept.getEmpList().add(emp);
        }
        DB.save(dept);
    }

    /**
     *
     * 级联修改
     */
    static void  update(){
        List<Dept> list = DB.find(Dept.class).where().findList();
        for(Dept dept :list){
            dept.setName("修改部门名称");
            List<Emp> empList = dept.getEmpList();
            for(Emp emp:empList){
                emp.setName("修改员工姓名");
            }
            DB.update(dept);
        }
    }

    /**
     *
     * 修改主表，插入副表
     */
    static void  updateAndSave(){
        List<Dept> list = DB.find(Dept.class).where().findList();
        for(Dept dept :list){
            dept.setName("修改部门名称2");
            for(int i=0;i<2;i++){
                Emp emp =new Emp();
                emp.setName("tech");
                emp.setNo("tech");
                emp.setDept(dept);
                dept.getEmpList().add(emp);
            }
            DB.update(dept);
        }
    }

    /**
     *
     * 级联查询
     */
    static void deptSearch(){
        List<Dept> list = DB.find(Dept.class).fetch("empList").findList();
        System.out.println(list);
        for(Dept dept:list){
            System.out.println(dept.getEmpList());
        }
    }



    /**
     *
     * or查询
     */
    static void orSearch(){
        ExpressionList<Dept> el = DB.find(Dept.class).fetch("empList").where();
        el.or(Expr.eq("no",1), Expr.eq("no",2));
        System.out.println(el.findList());
    }


    /**
     *
     * in子查询
     */
    static void subSearch(){
        Query<Dept> query = DB.find(Dept.class).select("no").where().eq("no", 1).query();
        ExpressionList<Dept> el = DB.find(Dept.class).fetch("empList").where();
        el.in("no", query);
        System.out.println(el.findList());
    }

    /**
     *
     * 原生防sql注入
     */
    static void noSqlIn(){
        String sql = "update tbl_dept set name=:name where no=:no";
        int execute = DB.createSqlUpdate(sql)
                .setParameter("name", "防止sql注入")
                .setParameter("no", 1).execute();
        System.out.println(execute);
    }

    /**
     *
     * 多对多级联插入
     */
    static void manyToManySave(){
       /* Teacher teacher =new Teacher();
        teacher.setName("Salter");
        teacher.setTeacherNo("teacher");
        for(int i=0;i<2;i++){
            Student student=new Student();
            student.setName("学生");
            student.setStudentNo("student");
            student.getTeacher().add(teacher);
            teacher.getStudents().add(student);
        }

        DB.save(teacher);*/
    }


}
