package com.hongru.constant;

/**
* @Description
* @Copyright (c) 1998-2022 北京新鸿儒世纪网络技术有限公司 All Rights Reserved.
* @Url https://www.xinhongru.com
* @ClassName DictConstant
* @Author salter <salter@vip.163.com>
* @Version V1.0.0
* @Since 1.0
* @Date 2022/4/22 10:01
*/
public interface DictConstant {

        /**
        * 完成状态
        */
        public static final Integer COMPLETION_STATUS_1 = 1;//未完成
        public static final Integer COMPLETION_STATUS_2 = 2;//已完成
        /**
        * 问卷类型
        */
        public static final Integer QUESTION_TYPE_1 = 1;//整体医学问卷
        public static final Integer QUESTION_TYPE_2 = 2;//中医体质问卷
        /**
        * 状态
        */
        public static final Integer STATUS_0 = 0;//关闭
        public static final Integer STATUS_1 = 1;//开启
        /**
        * 点赞收藏操作类型
        */
        public static final String LIKE_OPERATION_LIKE = "like";//点赞
        public static final String LIKE_OPERATION_COLLECT = "collect";//收藏
        public static final String LIKE_OPERATION_SHARE = "share";//分享
        /**
        * 发送状态
        */
        public static final Integer SEND_STATUS_0 = 0;//未发送
        public static final Integer SEND_STATUS_1 = 1;//已发送
        /**
        * 测试
        */
        public static final String 测_擦擦擦 = "擦擦擦";//测测
        /**
        * 开关
        */
        public static final Integer IS_OPEN_0 = 0;//否
        public static final Integer IS_OPEN_1 = 1;//是
        /**
        * 优先级
        */
        public static final String PRIORITY_L = "L";//低
        public static final String PRIORITY_M = "M";//中
        public static final String PRIORITY_H = "H";//高
        /**
        * 阅读状态
        */
        public static final Integer READ_STATUS_0 = 0;//未读
        public static final Integer READ_STATUS_1 = 1;//已读
        /**
        * 测试
        */
        public static final String 测试_测试2 = "测试2";//测试1
        /**
        * 健康问卷问题类型
        */
        public static final Integer PROBLEM_TYPE_1 = 1;//单选
        public static final Integer PROBLEM_TYPE_2 = 2;//多选
        public static final Integer PROBLEM_TYPE_4 = 4;//数字
        public static final Integer PROBLEM_TYPE_3 = 3;//文本
        /**
        * 动态类型
        */
        public static final String DYNAMIC_TYPE_TXT = "txt";//文本
        public static final String DYNAMIC_TYPE_IMG = "img";//图片
        public static final String DYNAMIC_TYPE_VIDEO = "video";//视频
        /**
        * 报告完成状态
        */
        public static final Integer MR_STATUS_0 = 0;//未完成
        public static final Integer MR_STATUS_1 = 1;//进行中
        public static final Integer MR_STATUS_2 = 2;//已完成
        /**
        * 表类型
        */
        public static final Integer TABLE_TYPE_1 = 1;//单表
        public static final Integer TABLE_TYPE_2 = 2;//主表
        public static final Integer TABLE_TYPE_3 = 3;//附表
        /**
        * 参课方式
        */
        public static final String JOIN_MODE_ONLINE = "online";//线上
        public static final Integer JOIN_MODE_1 = 1;//测试
        public static final String JOIN_MODE_OFFLINE = "offline";//线下
        /**
        * 性别
        */
        public static final Integer SEX_0 = 0;//未知
        public static final Integer SEX_1 = 1;//男
        public static final Integer SEX_2 = 2;//女
        /**
        * 点赞收藏类型
        */
        public static final String LIKE_TYPE_UMSDYNAMIC = "UmsDynamic";//动态消息
        public static final String LIKE_TYPE_CMSCONTENT = "CmsContent";//内容
        public static final String LIKE_TYPE_UMSCOMMENT = "UmsComment";//评论
        public static final String LIKE_TYPE_UMSQA = "UmsQa";//健康问答
        /**
        * 填写状态
        */
        public static final Integer MEMBER_QUESTION_STATUS_1 = 1;//已填写
        public static final Integer MEMBER_QUESTION_STATUS_0 = 0;//填写中
        /**
        * 消息状态
        */
        public static final Integer MESSAGE_STATUS_1 = 1;//未读
        public static final Integer MESSAGE_STATUS_2 = 2;//已读
        /**
        * 话题
        */
        public static final Integer TOPIC_1 = 1;//日常动态
        public static final Integer TOPIC_2 = 2;//饮食打卡
        /**
        * 周期
        */
        public static final String PERIOD_WEEK = "week";//周
        public static final String PERIOD_MONTH = "month";//月
        public static final String PERIOD_YEAR = "year";//年
        /**
        * 云服务商
        */
        public static final String CLOUD_TYPE_ALICLOUD = "alicloud";//阿里云
        public static final String CLOUD_TYPE_QCLOUD = "qcloud";//腾讯云
        /**
        * 会员类型
        */
        public static final Integer MEMBER_TYPE_0 = 0;//会员
        public static final Integer MEMBER_TYPE_1 = 1;//虚拟会员
        /**
        * 报告版本
        */
        public static final Integer MR_TYPE_1 = 1;//基础版
        public static final Integer MR_TYPE_2 = 2;//定制版
        /**
        * 接口调用状态
        */
        public static final Integer REQ_STATUS_1 = 1;//成功
        public static final Integer REQ_STATUS_0 = 0;//失败
        /**
        * 操作类型
        */
        public static final String OPERATE_TYPE_ADD = "add";//添加
        public static final String OPERATE_TYPE_EDIT = "edit";//编辑
        public static final String OPERATE_TYPE_DELETE = "delete";//删除
        /**
        * 测
        */
        /**
        * 1是0否
        */
        public static final Integer YN_0 = 0;//否
        public static final Integer YN_1 = 1;//是
        /**
        * 评论类型
        */
        public static final String COMMENT_TYPE_UMSDYNAMIC = "UmsDynamic";//动态
        public static final String COMMENT_TYPE_CMSCONTENT = "CmsContent";//内容
        public static final String COMMENT_TYPE_UMSCOMMENT = "UmsComment";//评论
        /**
        * 列表显示类型
        */
        public static final Integer CONTENT_LIST_TYPE_1 = 1;//文字
        public static final Integer CONTENT_LIST_TYPE_2 = 2;//图文
        public static final Integer CONTENT_LIST_TYPE_3 = 3;//视频
        public static final Integer CONTENT_LIST_TYPE_4 = 4;//音频
        /**
        * 打卡模板类型
        */
        public static final String CLOCKIN_TEMPLATE_TYPE_M1 = "m1";//体重模板
        public static final String CLOCKIN_TEMPLATE_TYPE_M2 = "m2";//运动模板
        /**
        * 订阅类型
        */
        public static final Integer SUBSCRIBE_TYPE_1 = 1;//课程
        public static final Integer SUBSCRIBE_TYPE_0 = 0;//普通
        /**
        * 审核状态
        */
        public static final Integer AUDIT_STATUS_0 = 0;//待审核
        public static final Integer AUDIT_STATUS_1 = 1;//审核通过
        public static final Integer AUDIT_STATUS_2 = 2;//审核未通过

}
