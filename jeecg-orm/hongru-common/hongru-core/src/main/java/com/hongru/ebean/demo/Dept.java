package com.hongru.ebean.demo;

import com.hongru.ebean.HongRuEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "demo_dept")
@Data
public class Dept extends HongRuEntity{

    String name;
    String no;
    @OneToMany
    List<Emp> empList = new ArrayList<>();

}
