import { getAction, deleteAction, putAction, postAction, httpAction } from '@/api/manage'
import Vue from 'vue'
import { UI_CACHE_DB_DICT_DATA } from '@/store/mutation-types'

//链接管理
const addChannel = (params) => postAction('/cms/channel/add', params)
const editChannel = (params) => postAction('/cms/channel/edit', params)


//链接管理
const addLink = (params) => postAction('/sys/link/add', params)
const editLink = (params) => postAction('/sys/link/edit', params)


//健康动态管理
const addDynamic = (params) => postAction('/ums/dynamic/add', params)
const editDynamic = (params) => postAction('/ums/dynamic/edit', params)

//健康问题管理
const addQa = (params) => postAction('/ums/qa/add', params)
const editQa = (params) => postAction('/ums/qa/edit', params)


//短信模板管理
const addSmsTemplate = (params) => postAction('/mms/sms/template/add', params)
const editSmsTemplate = (params) => postAction('/mms/sms/template/edit', params)


//短信计划管理
const addSmsTask = (params) => postAction('/mms/sms/task/add', params)
const editSmsTask = (params) => postAction('/mms/sms/task/edit', params)

const addVirtualMember = (params) => postAction('/ums/virtual/member/add', params)
const editVirtualMember = (params) => postAction('/ums/virtual/member/edit', params)

//健康课堂管理
const addMember = (params) => postAction('/ums/member/add', params)
const editMember = (params) => postAction('/ums/member/edit', params)

//运动康复计划管理
const addExercisePlan = (params) => postAction('/ums/exercise/plan/add', params)
const editExercisePlan = (params) => postAction('/ums/exercise/plan/edit', params)
const addExercisePlanDetail = (params) => postAction('/ums/exercise/plan/detail/add', params)
const editExercisePlanDetail = (params) => postAction('/ums/exercise/plan/detail/edit', params)

//运动视频管理
const addVideo = (params) => postAction('/cms/video/add', params)
const editVideo = (params) => postAction('/cms/video/edit', params)

//医学报告管理
const addMr = (params) => postAction('/ums/mr/admin/add', params)
const editMr = (params) => postAction('/ums/mr/edit', params)
const addMrContent = (params) => postAction('/ums/mr/content/add', params)
const editMrContent = (params) => postAction('/ums/mr/content/edit', params)

//身体检测管理
const addPe = (params) => postAction('/ums/pe/add', params)
const editPe = (params) => postAction('/ums/pe/edit', params)

//健康课堂管理
const addCourse = (params) => postAction('/cms/course/add', params)
const editCourse = (params) => postAction('/cms/course/edit', params)

//打卡模板
const addClockInTemplate = (params)=>postAction("/ums/clock/in/template/add",params);
const editClockInTemplate = (params)=>postAction("/ums/clock/in/template/edit",params);

//健康问卷管理
const addQuestion = (params)=>postAction("/cms/question/add",params);
const editQuestion = (params)=>postAction("/cms/question/edit",params);

//健康问卷问题管理
const addQuestionProblem = (params)=>postAction("/cms/question/problem/add",params);
const editQuestionProblem = (params)=>postAction("/cms/question/problem/edit",params);

//站内信管理
const addSiteMessage = (params)=>postAction("/mms/site/message/add",params);
const editSiteMessage = (params)=>postAction("/mms/site/message/edit",params);

//订阅管理
const addSubscribeTemplate = (params)=>postAction("/mms/subscribe/template/add",params);
const editSubscribeTemplate = (params)=>postAction("/mms/subscribe/template/edit",params);

//内容管理
const addContent = (params) => postAction('/cms/content/add', params)
const editContent = (params) => postAction('/cms/content/edit', params)

//代码生成
const addTable = (params) => postAction('/gen/table/add', params)
const editTable = (params) => postAction('/gen/table/edit', params)
const queryColumnList = (params) => postAction('/gen/tableColumn/list', params)

//项目管理
const addLbs = (params) => postAction('/sys/lbs/add', params)
const editLbs = (params) => postAction('/sys/lbs/edit', params)

//角色管理
const addRole = (params) => postAction('/sys/role/add', params)
const editRole = (params) => postAction('/sys/role/edit', params)
const checkRoleCode = (params) => getAction('/sys/role/checkRoleCode', params)
const roleList = (params) => postAction('/sys/role/list', params)

//用户管理
const addUser = (params) => postAction('/sys/user/add', params)
const editUser = (params) => postAction('/sys/user/edit', params)
const queryUserRole = (params) => getAction('/sys/user/queryUserRole', params)
const getUserList = (params) => getAction('/sys/user/list', params)
const frozenBatch = (params) => putAction('/sys/user/frozenBatch', params)
//验证用户是否存在
const checkOnlyUser = (params) => getAction('/sys/user/checkOnlyUser', params)
//改变密码
const changePassword = (params) => postAction('/sys/user/changePassword', params)

//权限管理
const addPermission = (params) => postAction('/sys/permission/add', params)
const editPermission = (params) => postAction('/sys/permission/edit', params)
const getPermissionList = (params) => getAction('/sys/permission/list', params)
const getSystemMenuList = (params) => getAction('/sys/permission/getSystemMenuList', params)
const getSystemSubmenu = (params) => getAction('/sys/permission/getSystemSubmenu', params)
const getSystemSubmenuBatch = (params) => getAction('/sys/permission/getSystemSubmenuBatch', params)
const queryTreeList = (params) => getAction('/sys/permission/queryTreeList', params)
const queryTreeListForRole = (params) => getAction('/sys/role/queryTreeList', params)
const queryListAsync = (params) => getAction('/sys/permission/queryListAsync', params)
const queryRolePermission = (params) => getAction('/sys/permission/queryRolePermission', params)
const saveRolePermission = (params) => postAction('/sys/permission/saveRolePermission', params)
const queryPermissionsByUser = () => getAction('/sys/permission/getUserPermission')
const loadAllRoleIds = (params) => getAction('/sys/permission/loadAllRoleIds', params)
const getPermissionRuleList = (params) => getAction('/sys/permission/getPermRuleListByPermId', params)
const queryPermissionRule = (params) => getAction('/sys/permission/queryPermissionRule', params)

// 部门管理
const queryDepartTreeList = (params) => getAction('/sys/sysDepart/queryTreeList', params)
const queryDepartTreeSync = (params) => getAction('/sys/sysDepart/queryDepartTreeSync', params)
const queryIdTree = (params) => getAction('/sys/sysDepart/queryIdTree', params)
const queryParentName = (params) => getAction('/sys/sysDepart/queryParentName', params)
const searchByKeywords = (params) => getAction('/sys/sysDepart/searchBy', params)
const deleteByDepartId = (params) => deleteAction('/sys/sysDepart/delete', params)

//二级部门管理
const queryDepartPermission = (params) => getAction('/sys/permission/queryDepartPermission', params)
const saveDepartPermission = (params) => postAction('/sys/permission/saveDepartPermission', params)
const queryTreeListForDeptRole = (params) => getAction('/sys/sysDepartPermission/queryTreeListForDeptRole', params)
const queryDeptRolePermission = (params) => getAction('/sys/sysDepartPermission/queryDeptRolePermission', params)
const saveDeptRolePermission = (params) => postAction('/sys/sysDepartPermission/saveDeptRolePermission', params)
const queryMyDepartTreeList = (params) => getAction('/sys/sysDepart/queryMyDeptTreeList', params)

//日志管理
const deleteLog = (params) => deleteAction('/sys/log/delete', params)
const deleteLogList = (params) => deleteAction('/sys/log/deleteBatch', params)

//数据字典
const addDict = (params) => postAction('/sys/dict/add', params)
const editDict = (params) => postAction('/sys/dict/edit', params)
const treeList = (params) => getAction('/sys/dict/treeList', params)
const addDictItem = (params) => postAction('/sys/dictItem/add', params)
const editDictItem = (params) => postAction('/sys/dictItem/edit', params)

//字典标签专用（通过code获取字典数组）
export const ajaxGetDictItems = (code, params) => getAction(`/sys/dict/getDictItems/${code}`, params)

//从缓存中获取字典配置
function getDictItemsFromCache(dictCode) {
  if (Vue.ls.get(UI_CACHE_DB_DICT_DATA) && Vue.ls.get(UI_CACHE_DB_DICT_DATA)[dictCode]) {
    let dictItems = Vue.ls.get(UI_CACHE_DB_DICT_DATA)[dictCode]
    //console.log("-----------getDictItemsFromCache----------dictCode="+dictCode+"---- dictItems=",dictItems)
    return dictItems
  }
}

//系统通告
const doReleaseData = (params) => getAction('/sys/annountCement/doReleaseData', params)
const doReovkeData = (params) => getAction('/sys/annountCement/doReovkeData', params)
//获取系统访问量
const getLoginfo = (params) => getAction('/sys/loginfo', params)
const getVisitInfo = (params) => getAction('/sys/visitInfo', params)

// 根据部门主键查询用户信息
const queryUserByDepId = (params) => getAction('/sys/user/queryUserByDepId', params)

// 重复校验
const duplicateCheck = (params) => getAction('/sys/duplicate/check', params)
// 加载分类字典
const loadCategoryData = (params) => getAction('/sys/category/loadAllData', params)
const checkRuleByCode = (params) => getAction('/sys/checkRule/checkByCode', params)
//加载我的通告信息
const getUserNoticeInfo = (params) => getAction('/sys/sysAnnouncementSend/getMyAnnouncementSend', params)
const getTransitURL = url => `/sys/common/transitRESTful?url=${encodeURIComponent(url)}`
// 中转HTTP请求
export const transitRESTful = {
  get: (url, parameter) => getAction(getTransitURL(url), parameter),
  post: (url, parameter) => postAction(getTransitURL(url), parameter),
  put: (url, parameter) => putAction(getTransitURL(url), parameter),
  http: (url, parameter) => httpAction(getTransitURL(url), parameter)
}

export {
  addQa,
  editQa,
  addChannel,
  editChannel,
  addLink,
  editLink,
  addDynamic,
  editDynamic,
  addSmsTemplate,
  editSmsTemplate,
  addSmsTask,
  editSmsTask,
  addMember,
  editMember,
  addVirtualMember,
  editVirtualMember,
  addExercisePlan,
  editExercisePlan,
  addExercisePlanDetail,
  editExercisePlanDetail,
  addVideo,
  editVideo,
  addMr,
  editMr,
  addMrContent,
  editMrContent,
  addPe,
  editPe,
  addCourse,
  editCourse,
  addClockInTemplate,
  editClockInTemplate,
  addQuestion,
  editQuestion,
  addQuestionProblem,
  editQuestionProblem,
  addSiteMessage,
  editSiteMessage,
  addSubscribeTemplate,
  editSubscribeTemplate,
  addContent,
  editContent,
  addLbs,
  editLbs,
  addTable,
  editTable,
  queryColumnList,
  // imgView,
  // doMian,
  addRole,
  editRole,
  checkRoleCode,
  addUser,
  editUser,
  queryUserRole,
  getUserList,
  roleList,
  frozenBatch,
  checkOnlyUser,
  changePassword,
  getPermissionList,
  addPermission,
  editPermission,
  queryTreeList,
  queryListAsync,
  queryRolePermission,
  saveRolePermission,
  queryPermissionsByUser,
  loadAllRoleIds,
  getPermissionRuleList,
  queryPermissionRule,
  queryDepartTreeList,
  queryDepartTreeSync,
  queryIdTree,
  queryParentName,
  searchByKeywords,
  deleteByDepartId,
  deleteLog,
  deleteLogList,
  addDict,
  editDict,
  treeList,
  addDictItem,
  editDictItem,
  doReleaseData,
  doReovkeData,
  getLoginfo,
  getVisitInfo,
  queryUserByDepId,
  duplicateCheck,
  queryTreeListForRole,
  getSystemMenuList,
  getSystemSubmenu,
  getSystemSubmenuBatch,
  loadCategoryData,
  checkRuleByCode,
  queryDepartPermission,
  saveDepartPermission,
  queryTreeListForDeptRole,
  queryDeptRolePermission,
  saveDeptRolePermission,
  queryMyDepartTreeList,
  getUserNoticeInfo,
  getDictItemsFromCache
}



